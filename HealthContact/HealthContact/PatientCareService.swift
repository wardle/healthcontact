//
//  RsdbService.swift
//  PatientContact
//
//  Created by Mark Wardle on 22/06/2014.
//  Copyright (c) 2014 Eldrix. All rights reserved.
//

import Foundation

/**
 A real live RSDB service which takes a base URL as parameter during initialisation.
 */
open class PatientCareService : RsdbService {
    
    let baseUrl : URL
    let dateFormat = "yyyy-MM-dd"
    let dateFormatter = DateFormatter()
    public let usePseudonyms : Bool

    var user : User?
    var patientUser : User = User(id: 1, username: "system", firstNames: "System", lastName: "Administrator", email: "mark@wardle.org")
    
    var password : String?          // we cache the password in the event of further requests at re-authentication
    lazy var session : URLSession = URLSession(configuration: URLSessionConfiguration.default,
                                               delegate: UrlSessionDelegate(service: self), delegateQueue: nil)
    
    /// initialise the service using the specified base URL
    public init(baseUrl : URL, usePseudonyms: Bool) {
        self.baseUrl = baseUrl;
        self.dateFormatter.dateFormat = dateFormat
        self.usePseudonyms = usePseudonyms
    }
    
    /// convenience method to take a path and perform variable substitution using the params specified as well as append the optional query string
    func createUrl(_ path:String, params:[CVarArg]?, query:[String:String]? = nil) -> URL {
        var urlString = path
        if params != nil {
            urlString = String(format: path, arguments: params!)
        }
        var url = URL(string: urlString, relativeTo: self.baseUrl)
        if query != nil {
            url = WebServiceUtilities.NSURLByAppendingQueryParameters(url!, queryParameters: query!)
        }
        return url!
    }
    
    /**
     the url for the list users action.
     this optionally takes query parameters
     */
    func urlForUsers(_ query:[String:String]? = nil) -> URL {
        let path = "users"
        let url = URL(string: path, relativeTo: baseUrl)
        var urlComponents = URLComponents(url: url!, resolvingAgainstBaseURL: true)
        if query != nil && query!.count > 0 {
            var queryString = ""
            for (k,v) in query! {
                queryString += k
                queryString += "='"
                queryString += v
                queryString += "'"
            }
            urlComponents!.query = "queryString=" + queryString.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
        }
        return urlComponents!.url!
    }
    
    /**
     Take a set of query parameters and encode them into one query parameter
     This is used for passing parameters through to the server indirectly. The server expands a query parameter for "queryString"
     and uses it to qualify fetches (e.g. a list action)
     */
    func encodeURLQueryStringWithKey(_ key:String, query:[String:String]) -> [String:String] {
        var queryString = ""
        for (k,v) in query {
            queryString += k
            queryString += "='"
            queryString += v
            queryString += "'"
        }
        return [key: queryString.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!]
    }
    
    /// the url for the list patients action
    func urlForListPatientsForProject(_ project:Project, et:EncounterTemplate, onDate date : Date) -> URL {
        let patientsUrlPath = "projects/%d/encounterTemplates/%d/patientsForDate"
        let dateString = dateFormatter.string(from: date)     // currently unused as the service returns for today's date
        let url = URL(string: String(format: patientsUrlPath, project.id, et.id), relativeTo: baseUrl)
        var urlComponents = URLComponents(url:url!, resolvingAgainstBaseURL: true)
        urlComponents!.query = "date=\(dateString)"
        return urlComponents!.url!
    }
    
    /// url for the findOrRegisterPatient action
    func urlForFindOrRegisterPatient(_ patient: StubPatient, project: Project, encounterTemplate: EncounterTemplate) -> URL {
        let findOrRegisterPatientPath = "projects/%d/encounterTemplates/%d/findOrRegisterPatient"
        
        let url = URL(string: String(format: findOrRegisterPatientPath, arguments: [project.id, encounterTemplate.id]), relativeTo: baseUrl)
        return WebServiceUtilities.NSURLByAppendingQueryParameters(url,
                                                                   queryParameters: [
                                                                    "hospitalIdentifier" : patient.hospitalIdentifier,
                                                                    "forceNewRegistration": "1"
            ]
        )
    }
    
    
    /**
     Support for handling authentication challenges by the server.
     */
    class UrlSessionDelegate : NSObject, URLSessionDelegate {
        
        unowned let service : PatientCareService;
        
        init(service : PatientCareService) {
            self.service = service
        }
        /*
        //
        // handle authentication challenges (including SSL handshake)
        //
        func urlSession(_ session: URLSession, didReceive challenge: URLAuthenticationChallenge, completionHandler: @escaping (URLSession.AuthChallengeDisposition, URLCredential?) -> Void) {
            //let credential = challenge.proposedCredential
            NSLog("Received authentication challenge")
            //
            // we currently accept credential (SSL certificate) in development
            // TODO: this needs to be fixed for deployment.
            //
            if let u = service.user {
                completionHandler(Foundation.URLSession.AuthChallengeDisposition.useCredential,
                                  URLCredential(user: u.username, password: service.password!, persistence: URLCredential.Persistence.forSession))
            }
            else {
                NSLog("Error: unable to respond to authentication challenge as no current user.")
            }
        }
         */
    }
    
    /// Create a HTTP GET request to the given URL
    func httpGetRequestForURL(_ URL:Foundation.URL) -> URLRequest {
        let request = NSMutableURLRequest(url: URL)
        request.httpMethod = "GET"
        return request as URLRequest
    }
    
    func httpPostRequestForURL(_ URL:Foundation.URL) -> URLRequest {
        return httpPostRequestForURL(URL, withJsonData: nil)
    }
    
    /// Create a HTTP POST request to the given URL with JSON data (either NSArray or NSDictionary)
    func httpPostRequestForURL(_ URL:Foundation.URL, withJson: AnyObject?) throws -> URLRequest {
        var data : Data?
        if withJson != nil {
            data = try JSONSerialization.data(withJSONObject: withJson!, options: .prettyPrinted)
        }
        return httpPostRequestForURL(URL, withJsonData: data)
    }
    
    /// Create a HTTP POST request to the given URL with the data specified
    func httpPostRequestForURL(_ URL:Foundation.URL, withJsonData: Data?) -> URLRequest {
        let request = NSMutableURLRequest(url: URL)
        request.httpMethod = "POST"
        request.setValue("application/json", forHTTPHeaderField: "content-type")
        if withJsonData != nil {
            request.httpBody = withJsonData
            //print(NSString(data:request.HTTPBody!, encoding: NSUTF8StringEncoding))
        }
        return request as URLRequest
    }
    
    /// convenience method to take the typical response from a HTTP request response and parse into JSON
    func parseJsonFromData(_ data:Data?, error:Error?) -> (json:Any?, error:Error?) {
        do {
            if error != nil {
                return(json:nil, error:error)
            }
            else {
                let result = try JSONSerialization.jsonObject(with: data!, options: .mutableContainers)
                return(json: result, error: error)
            }
        }
        catch let error as NSError {
            return(json: nil, error: error)
        }
        catch {
            return(json: nil, error: NSError(domain: "Failed to parse JSON", code: 0, userInfo: nil))
        }
    }
    
    /// perform a HTTP task
    /// any errors are passed directly but specific HTTP errors are parsed, handled and turned into request errors.
    func performHttpRequest(_ request:URLRequest, onCompletion:@escaping(_ data:Data?, _ response:HTTPURLResponse?, _ error:Error?) -> Void) {
        let task = session.dataTask(with: request, completionHandler: {
            (data:Data?, response:URLResponse?, error:Error?) in
            if let _ = error {
                onCompletion(data, response as? HTTPURLResponse, error)
            }
            else {
                if let hResponse = response as? HTTPURLResponse {
                    let statusCode = hResponse.statusCode
                    switch(statusCode) {
                    case 300...600:
                        var localisedString : String?
                        if let data = data {
                            localisedString = String(data: data, encoding: String.Encoding.utf8)
                        } else {
                            localisedString = HTTPURLResponse.localizedString(forStatusCode: statusCode).uppercased()
                        }
                        onCompletion(data, hResponse, NSError(domain: localisedString!, code:statusCode, userInfo:hResponse.allHeaderFields as? [String : Any]))
                    default:
                        onCompletion(data, hResponse, error)
                    }
                }
            }
        })
        task.resume()
    }
    
    /// perform a HTTP task and parse the returned JSON data and convert into an object
    func performHttpRequestForObject<T>(_ request:URLRequest, creator:@escaping(NSDictionary) -> T?, onCompletion:@escaping (_ result:Result<T, Error>) -> Void) {
        performHttpRequest(request) {
            data, response, error in
            let (json, error) = self.parseJsonFromData(data, error: error as Error?)
            var err : Error? = error
            var object : T?
            if err == nil {
                if let dict = json as? NSDictionary {
                    object = creator(dict)
                }
                else {
                    err = RsdbError.jsonSerialization
                }
            }
            DispatchQueue.main.async {
                onCompletion(Result.result(object, err))
            }
        }
    }
    
    /// Attempt a login with the given credentials and perform the given callback (on the main thread) after successful login.
    public func loginWithUsername(_ username: String, password: String, onCompletion: @escaping(Result<User, Error>) -> Void) {
        self.password = password        // cache password for user in case of future authentication challenges
        let url = createUrl("users/login", params: nil, query: [
            "username" : username,
            "password" : password
            ])
        let request = httpGetRequestForURL(url)
        performHttpRequestForObject(request, creator: {User(data: $0)}) {
            result in
            if let user  = result.result {
                self.user = user
            }
            onCompletion(result)
        }
    }
    
    /// Convenience method to allow download of arbitrary lists of objects from a URL.
    func performHttpRequestForList<T>(request:URLRequest, createObjectFromDictionary:@escaping (NSDictionary) -> T?, onCompletion:@escaping (Result<[T], Error>) -> Void) {
        performHttpRequest(request) {
            (data:Data?, response:HTTPURLResponse?, error:Error?) in
            let (json, error) = self.parseJsonFromData(data, error: error)
            var err : Error? = error
            var resultList : Array = [T]()
            if err == nil {
                if let list = json as? NSArray {
                    for item in list {
                        if let dict = item as? NSDictionary {
                            let o = createObjectFromDictionary(dict)
                            if o != nil {
                                resultList.append(o!)
                            }
                            else {
                                err = RsdbError.jsonMapping
                                break
                            }
                        }
                        else {
                            err = RsdbError.jsonSerialization
                            break
                        }
                    }
                }
                else {
                    err = RsdbError.jsonSerialization
                }
            }
            if err != nil {
                debugPrint("Error: \(String(describing: err))")
                resultList = [T]()
            }
            DispatchQueue.main.async {
                onCompletion(Result.result(resultList, err))
            }
        }
    }
    
    
    /// Fetch a list of users
    open func fetchUsersWithQuery(_ query:[String:String], onCompletion:@escaping (Result<[User], Error>) -> Void) {
        let url = createUrl("users", params: nil, query: encodeURLQueryStringWithKey("queryString", query: query))
        performHttpRequestForList(request: httpGetRequestForURL(url), createObjectFromDictionary: { User(data: $0) }, onCompletion: onCompletion)
    }
    
    
    /// Fetch a list of patients for the given project for the given encounter template on the specified date.
    open func fetchPatientsForProject(_ project:Project, et:EncounterTemplate, onDate date:Date, onCompletion:@escaping(Result<[StubPatient], Error>) -> Void) {
        let url = urlForListPatientsForProject(project, et:et, onDate: date)
        performHttpRequestForList(request: httpGetRequestForURL(url), createObjectFromDictionary:{ StubPatient(data:$0)}, onCompletion: onCompletion)
    }
    
    /// fetch patient by looking up a pseudonym
    open func registerPatientUsingPseudonymToProject(_ project:Project, nhsNumber:String, dateBirth:Date, sex:Sex, registerIfNotFound:Bool, onCompletion:@escaping(Result<Patient, Error>)->Void) {
        let query = ["nhsNumber": nhsNumber,
                     "dateBirth": dateFormatter.string(from: dateBirth),
                     "sex"      : sex.rawValue,
            "registerIfNotFound" : String(registerIfNotFound)
        ]
        let url = createUrl("projects/%d/patients/register", params:[project.id], query: query)
        let request = httpPostRequestForURL(url)
        let creator : (NSDictionary) -> Patient = { data in Patient(data:data) }
        performHttpRequestForObject(request, creator:creator, onCompletion:onCompletion)

    }

    open func fetchPatientWithPseudonym(_ pseudonym:String, forProject project:Project, onCompletion:@escaping(Result<Patient, Error>)->Void) {
        let query = ["pseudonym":pseudonym]
        let url = createUrl("projects/%d/patients/search", params:[project.id], query: query)
        let request = httpGetRequestForURL(url);
        let creator : (NSDictionary) -> Patient = { data in Patient(data:data) }
        performHttpRequestForObject(request, creator:creator, onCompletion:onCompletion)    }

    
    /// Get a list of the projects suitable for this patient
    open func fetchResearchForPatient(_ patient: Patient, onCompletion: @escaping (Result<[Project], Error>) -> Void) {
        let url = createUrl("patients/%d/availableResearch", params: [patient.id])
        performHttpRequestForList(request: httpGetRequestForURL(url), createObjectFromDictionary:{Project(data:$0)}, onCompletion: onCompletion)
    }
    
    /// Fetch consent form for the given project with the specified identifier.
    open func fetchConsentForm(_ projectId:Int, consentFormId:Int, completion onCompletion:@escaping (Result<ConsentForm, Error>) -> Void) {
        let url = createUrl("projects/%d/consentForms/%d", params: [projectId, consentFormId])
        let request = httpGetRequestForURL(url)
        
        performHttpRequestForObject(request, creator:{ConsentForm(data:$0)}, onCompletion: onCompletion)
    }
    
    /// Fetch a consent form information leaflet from the remote server
    open func fetchConsentFormInformationLeaflet(_ projectId:Int, consentFormId:Int, onCompletion: @escaping (Result<Data, Error>) -> Void) {
        let url = createUrl("projects/%d/consentForms/%d/informationLeaflet", params: [projectId, consentFormId])
        performHttpRequest(httpGetRequestForURL(url)) {
            data, response, error in
            DispatchQueue.main.async {
                onCompletion(Result.result(data, error))
            }
        }
    }
    
    public func fetchLogoForProjectWithProjectIdentifier(_ projectId:Int, onCompletion:@escaping (Result<Data, Error>) -> Void) {
        let url = createUrl("projects/%d/logo", params: [projectId])
        performHttpRequest(httpGetRequestForURL(url)) {
            data, response, error in
            DispatchQueue.main.async {
                onCompletion(Result.result(data, error))
            }
        }
    }
    
    
    /**
     Find or create a new registration for the given patient
     */
    open func findOrRegisterPatient(_ patient: StubPatient, forProject project: Project, andEncounterTemplate encounterTemplate: EncounterTemplate, onCompletion: @escaping (Result<Patient, Error>) -> Void) {
        let url = createUrl("projects/%d/encounterTemplates/%d/findOrRegisterPatient", params:[project.id, encounterTemplate.id], query:[
            "hospitalIdentifier" : patient.hospitalIdentifier,
            "forceNewRegistration": "1"
            ])
        let request = try! httpPostRequestForURL(url, withJson: nil)        // won't throw an exception as no parsing of JSON required
        performHttpRequestForObject(request, creator: { Patient(data:$0)}, onCompletion: onCompletion)
    }
    
    
    /**
     Create an encounter on the remote server.
     Note: you should discard the encounter passed in and instead use
     the encounter that is returned.
     */
    open func createEncounter(_ encounter:Encounter, onCompletion: @escaping (Result<Encounter, Error>) -> Void) {
        let url = createUrl("patients/%d/encounters", params: [encounter.patient.id])
        do {
            let request = try httpPostRequestForURL(url, withJson: encounter.asDictionary())
            performHttpRequestForObject(request, creator: {
                Encounter(patient: encounter.patient, encounterTemplate: encounter.encounterTemplate, user:self.patientUser, data: $0)
            }, onCompletion: onCompletion)
        }
        catch {
            onCompletion(Result.withError(NSError(domain: "Could not serialise encounter to JSON", code: 0, userInfo: nil)))
        }
    }
    
    /// Update the patient using the data specified
    open func updatePatient(_ patient:Patient, data:Data) {
        let url = createUrl("patients/%d", params:[patient.id])
        performHttpRequest(httpPostRequestForURL(url, withJsonData: data)) {
            data, response, error in
            if response?.statusCode == 200 && error == nil {
                print("Successfully updated patient.")
            }
            else {
                print("Failed to update patient: \(String(describing: error))")
            }
        }
    }
    
    /// Publish form data for the encounter specified.
    open func publishForm(_ data:Data, forEncounter encounter:Encounter) {
        let url = createUrl("patients/%d/encounters/%d/forms",params: [encounter.patient.id, encounter.id!])
        print("Publishing data for form to \(url)")
        print(NSString(data: data, encoding: String.Encoding.utf8.rawValue)!)
        
        performHttpRequest(httpPostRequestForURL(url, withJsonData: data)) {
            data, response, error in
            if response?.statusCode == 200 && error == nil {
                print("Successfully created form to \(url)")
            }
            else {
                print("Failed to create form: \(String(describing: error))")
                print("http status code: \(String(describing: response?.statusCode))");
                print(HTTPURLResponse.localizedString(forStatusCode: response!.statusCode))
                if data != nil {
                    print(NSString(data: data!, encoding: String.Encoding.utf8.rawValue)!)
                }
            }
        }
    }
    
    
    /**
     Publish a complete set of forms (bundled as a FormSet) with the set the results specified
     for the encounter specified.
     
     This combines information from the forms and the results to magically publish everything
     to the external server.
     */
    open func publishFormset(_ forms:FormSet, withResults results:FormSetResults, forEncounter encounter:Encounter) {
        
        print("Publishing forms to web service...")
        for form in forms.forms {
            print("Publishing form: " + type(of: form).entityName)
            let resultsForForm : [FormResult] = form.processResults(results)
            for result : FormResult in resultsForForm {
                _publishFormData(form: form, data: result.data, forEncounter: encounter)
            }
        }
    }
    
    private func _publishFormData(form:Form, data:NSDictionary, forEncounter encounter:Encounter) {
        if (JSONSerialization.isValidJSONObject(data)) {
            let data = try! JSONSerialization.data(withJSONObject: data, options: .prettyPrinted)
            print(NSString(data: data, encoding: String.Encoding.utf8.rawValue)!)
            if let publisher = formPublishers[type(of: form).entityName] {
                publisher.publishForm(form, forEncounter: encounter, withData: data, toService: self)
            }
            else {
                print("Warning: No publisher registered for \(type(of: form).entityName)")
            }
        }
        else {
            print("Error: Unable to serialise data for form \(type(of: form).entityName)")
        }
    }
    
    private let formPublishers : [String : FormPublisher] = [
        FormEdss.entityName : StandardFormPublisher(),
        FormParkinsonNonMotor.entityName : StandardFormPublisher(),
        FormMsis29.entityName : StandardFormPublisher(),
        "Patient" : PatientPublisher(),
        FormEq5d.entityName : StandardFormPublisher(),
        ]
    
}

/// Helper class to publish the results of a form
protocol FormPublisher {
    func publishForm(_ form:Form, forEncounter encounter:Encounter, withData data: Data, toService service:PatientCareService)
}


/**
 A form publisher suitable for most forms, in which the form is published linked to the encounter.
 As such, this implementation is trivial.
 */
class StandardFormPublisher : FormPublisher {
    func publishForm(_ form:Form, forEncounter encounter:Encounter, withData data: Data, toService service:PatientCareService) {
        print(NSString(data: data, encoding: String.Encoding.utf8.rawValue)!)
        service.publishForm(data, forEncounter: encounter)
    }
}

/**
 A form publisher that will update a patient based on the contents of the form
 Note: the web service allows updates of only certain fields so this couldn't be used to say, change the name of the patient!
 */
class PatientPublisher : FormPublisher {
    func publishForm(_ form:Form, forEncounter encounter:Encounter, withData data: Data, toService service:PatientCareService) {
        let patient = encounter.patient
        service.updatePatient(patient, data: data)
    }
}


/// A group of helpful URL utilities.
class WebServiceUtilities {
    /**
     This creates a new query parameters string from the given NSDictionary. For
     example, if the input is @{@"day":@"Tuesday", @"month":@"January"}, the output
     string will be @"day=Tuesday&month=January".
     @param queryParameters The input dictionary.
     @return The created parameters string.
     */
    class func stringFromQueryParameters(_ queryParameters : Dictionary<String, String>) -> String {
        var parts: [String] = []
        let allowed = CharacterSet.urlQueryAllowed
        for (name, value) in queryParameters {
            let part = NSString(format: "%@=%@",
                                name.addingPercentEncoding(withAllowedCharacters: allowed)!,
                                value.addingPercentEncoding(withAllowedCharacters: allowed)!)
            parts.append(part as String)
        }
        return parts.joined(separator: "&")
    }
    
    /**
     Creates a new URL by adding the given query parameters.
     @param URL The input URL.
     @param queryParameters The query parameter dictionary to add.
     @return A new NSURL.
     */
    class func NSURLByAppendingQueryParameters(_ URL : Foundation.URL!, queryParameters : Dictionary<String, String>) -> Foundation.URL {
        let URLString : NSString = NSString(format: "%@?%@", URL.absoluteString, WebServiceUtilities.stringFromQueryParameters(queryParameters))
        return Foundation.URL(string: URLString as String)!
    }
    
}



