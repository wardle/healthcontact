//
//  AppDelegate.swift
//  HealthContact
//
//  Created by Mark Wardle on 17/12/2016.
//  Copyright © 2016 Eldrix. All rights reserved.
//

import UIKit
import ResearchKit




struct Configuration {
    static let BASE_URL = "baseURL"
    static let USE_PSEUDONYMS = "usePseudonyms"
    static let PROJECT_ID = "projectID";
    static let PROJECT_TITLE = "projectTitle";
    static let ENCOUNTER_TEMPLATE_ID = "encounterTemplateID"
    static func baseUrl(config:NSDictionary) -> String {
        return config[BASE_URL] as! String
    }
    
    static func usePseudonyms(config:NSDictionary) -> Bool {
        return config[USE_PSEUDONYMS] as! Bool
    }
    static func projectID(config:NSDictionary) -> Int {
        return config[PROJECT_ID] as! Int
    }
    static func projectTitle(config:NSDictionary) -> String {
        return config[PROJECT_TITLE] as! String
    }
    static func encounterTemplateID(config:NSDictionary) -> Int {
        return config[ENCOUNTER_TEMPLATE_ID] as! Int
    }
}

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var window: UIWindow?
    
    var completionDelegate : QuestionnaireCompletionDelegate?
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        // what is our runtime configuration?
        let hcEnv = Bundle.main.object(forInfoDictionaryKey: "HC_ENVIRONMENT") as? String
        guard let env = hcEnv else {
            fatalError("Could not determine runtime configuration. Was HC_ENVIRONMENT runtime variable set?")
        }
        debugPrint("HealthContact: using configuration" + env)
        var hcConfig : NSDictionary?
        if let path = Bundle.main.path(forResource: "Config", ofType: "plist") {
            if let dict = NSDictionary(contentsOfFile: path) as? [String: AnyObject] {
                hcConfig = (dict[env] as? NSDictionary)
            }
        }
        guard let config = hcConfig else {
            fatalError("No configuration found for '" + env + "'")
        }
        
        let service = PatientCareService(baseUrl: URL(string:Configuration.baseUrl(config: config))!,
                                         usePseudonyms: Configuration.usePseudonyms(config: config))
        //let project = Project(identifier:5, title:"Cardiff neuro-inflammatory clinic")
        let project = Project(identifier:Configuration.projectID(config: config),
                              title:Configuration.projectTitle(config: config))
        let encounterTemplate = EncounterTemplate(identifier: Configuration.encounterTemplateID(config:config), project:project)  // Rookwood clinic
        //let encounterTemplate = EncounterTemplate(identifier: 153, project:project)  // MS clinic
        
        self.window = UIWindow(frame: UIScreen.main.bounds)
        let loginStoryboard: UIStoryboard = UIStoryboard(name: "Login", bundle: nil)
        let loginController = loginStoryboard.instantiateViewController(withIdentifier: "LoginController") as! LoginController
        loginController.service = service
        let loginNav = UINavigationController(rootViewController: loginController)
        self.window?.rootViewController = loginNav
        self.window?.makeKeyAndVisible()
        loginController.projectTitle = project.title
        loginController.loginAction = {
            (user, service) in
            //self.performQuestionnairesViaPseudonym(loginNav: loginNav, user:user, service:service, encounterTemplate: encounterTemplate)
            self.fetchPatientList(loginNav: loginNav, user: user, service: service, project: project, encounterTemplate: encounterTemplate)
        }
        return true
    }
    
    
    func fetchPatientList(loginNav:UINavigationController, user:User, service:RsdbService, project:Project, encounterTemplate:EncounterTemplate) {
        let chooseListStoryboard: UIStoryboard = UIStoryboard(name: "ChooseList", bundle: nil)
        let chooseList = chooseListStoryboard.instantiateViewController(withIdentifier: "ChooseListViewController") as! ChooseListViewController
        service.fetchPatientsForProject(project, et: encounterTemplate, onDate: Date()) {
            result in
            if let patients = result.result {
                chooseList.service = service
                chooseList.patients = patients
                chooseList.encounterTemplate = encounterTemplate
                loginNav.pushViewController(chooseList, animated: true)
                chooseList.completion = {
                    (patient) in
                    print("Starting questionnaires for patient: \(patient.patientIdentifier)")
                    
                    let pleaseWaitAlert = Utility.createBusyAlertWithTitle("Please wait", message: "Loading questionnaires...")
                    chooseList.present(pleaseWaitAlert, animated: true) {
                        self.doQuestionnaires(service: service, patient: patient, user:user, encounterTemplate:encounterTemplate, navigation: loginNav) {
                            // back to login page
                            loginNav.popToRootViewController(animated: true)
                        }
                    }
                }
            }
        }
    }
    
    func performQuestionnairesViaPseudonym(loginNav:UINavigationController, user:User, service:RsdbService, encounterTemplate:EncounterTemplate) {
        let choosePseudonymStoryboard: UIStoryboard = UIStoryboard(name: "ChoosePseudonym", bundle: nil)
        let choosePseudonym = choosePseudonymStoryboard.instantiateViewController(withIdentifier: "ChoosePseudonymPatientViewController") as! ChoosePseudonymPatientViewController
        choosePseudonym.service = service
        loginNav.pushViewController(choosePseudonym, animated: true)
        choosePseudonym.project = encounterTemplate.project
        choosePseudonym.completion = {
            (patient) in
            print("Starting questionnaires for patient: \(patient.patientIdentifier)")
            
            let pleaseWaitAlert = Utility.createBusyAlertWithTitle("Please wait", message: "Loading questionnaires...")
            loginNav.present(pleaseWaitAlert, animated: true) {
                self.doQuestionnaires(service: service, patient: patient, user:user, encounterTemplate:encounterTemplate, navigation: loginNav) {
                    // back to login page
                    loginNav.popToRootViewController(animated: true)
                }
            }
        }
    }
    
    
    func doQuestionnaires(service:RsdbService, patient:Patient, user:User, encounterTemplate:EncounterTemplate, navigation:UINavigationController, onCompletion:@escaping () -> Void) {
        service.fetchConsentForm(59, consentFormId: 16) {
            result in
            let consentForm : ConsentForm? = result.result
            var forms = [Form]()
            //forms.append(FormSmoking())
            forms.append(FormEq5d())
            //forms.append(FormPsat())
            forms.append(FormEdss())
            forms.append(FormMsis29())
            //forms.append(FormHolePeg())
            //forms.append(FormParkinsonNonMotor())
            forms.append(FormTapping())
            let researchInterested = FormResearchInterested(title: "Are you interested in taking part in research?")
            if patient.interestedInResearch != ResearchConsent.DISAGREE {
                if patient.interestedInResearch == ResearchConsent.NOT_ASKED ||
                    patient.interestedInResearch == ResearchConsent.UNDECIDED {
                    forms.append(researchInterested)
                }
            }
            forms.append(researchInterested)
            var wnrtbConsent : FormConsent?
            if consentForm != nil {
                wnrtbConsent = FormConsent(forPatient:patient, withConsent: consentForm!)
                if patient.isRegisteredOrReferredToProject(Project(identifier: 59, title: "WNRTB")) == false {
                    forms.append(wnrtbConsent!)
                }
            }
            //forms.append(FormParkinsonNonMotor())
            forms.append(FormCompleted())
            let formSet = FormSet( forms: forms)
            var steps = [ORKStep]()
            //      steps.append(introductionStep)
            steps += formSet.steps
            let task = ORKNavigableOrderedTask(identifier: "task", steps: steps)
            
            /// add all form navigation steps
            for (identifier, rule) in formSet.navigation {
                task.setNavigationRule(rule, forTriggerStepIdentifier: identifier)
            }
            /// if a patient is interested in research, show them the WNRTB information and consent forms
            if wnrtbConsent != nil {
                let researchInterestedIdentifier = researchInterested.formSteps[0].identifier
                let isResearchInterestedPredicate = ORKResultPredicate.predicateForChoiceQuestionResult(with: ORKResultSelector(resultIdentifier: researchInterestedIdentifier), expectedAnswerValue: ResearchConsent.AGREE.rawValue as NSString)
                let wnrtbRule = ORKPredicateStepNavigationRule(resultPredicates: [ isResearchInterestedPredicate ],
                                                               destinationStepIdentifiers: [wnrtbConsent!.formSteps[0].identifier], defaultStepIdentifier: formSet.identifierOfFirstStepOfNextForm(wnrtbConsent!), validateArrays:true)
                task.setNavigationRule(wnrtbRule, forTriggerStepIdentifier: researchInterestedIdentifier)
            }
            // and now go!
            let taskViewController = ORKTaskViewController(task: task, taskRun: nil)
            self.completionDelegate = QuestionnaireCompletionDelegate(service: service, user: user, patient: patient, formSet: formSet, encounterTemplate: encounterTemplate) {
                navigation.dismiss(animated: true) {
                    onCompletion()
                }
            }
            taskViewController.delegate = self.completionDelegate
            navigation.dismiss(animated: true) {
                navigation.present(taskViewController, animated: false, completion: nil)
            }
        }
        
    }
    
    
    // Delegate called when the ResearchKit framework has completed its task.
    // This publishes the data to the server.
    public class QuestionnaireCompletionDelegate : NSObject, ORKTaskViewControllerDelegate {
        
        let service:RsdbService
        let user:User
        let patient:Patient
        let formSet:FormSet
        let encounterTemplate:EncounterTemplate
        let completion : () -> Void
        
        init(service:RsdbService, user:User, patient:Patient, formSet:FormSet, encounterTemplate:EncounterTemplate, completion:@escaping () -> Void) {
            self.service = service
            self.user = user
            self.patient = patient
            self.formSet = formSet
            self.encounterTemplate = encounterTemplate
            self.completion = completion
        }
        public func taskViewController(_ taskViewController: ORKTaskViewController, didFinishWith reason:ORKTaskViewControllerFinishReason, error: Error?) {
            let taskResult : ORKTaskResult = taskViewController.result
            let results = FormSetResults(result: taskResult)
            
            var encounter = Encounter(patient: patient, encounterTemplate: encounterTemplate, user: user)
            if let duration = results.totalDurationSeconds {
                encounter.durationMinutes = Int(duration / 60)
            }
            service.createEncounter(encounter) {
                result in
                if let encounter = result.result {
                    self.service.publishFormset(self.formSet, withResults: results, forEncounter: encounter)
                }
            }
            completion()
        }
        
    }
    
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }
    
    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }
    
    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }
    
    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
    
    
}
