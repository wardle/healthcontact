//
//  RsdbModel.swift
//  HealthContactCore
//
//  Created by Mark Wardle on 10/12/2016.
//  Copyright © 2016 Eldrix. All rights reserved.
//

import Foundation


public class NhsNumber {
    public class func validateNhsNumber(_ nnn:String) -> Bool {
        if nnn.count > 9 {
            let nnn = nnn.components(separatedBy: .whitespaces).joined(separator: "")
            if nnn.count == 10 {
                var sum = 0
                var cd = 0
                for i in 0...9 {
                    let char = nnn[nnn.index(nnn.startIndex, offsetBy: i)]
                    if let n = Int(String(describing: char)) {
                        if (i < 9) {
                            sum += n * (10 - i)
                        } else {
                            cd = n
                        }
                    } else {
                        return false
                    }
                }
                var ccd = 11 - (sum % 11)
                if (ccd == 11) {
                    ccd = 0
                }
                if (ccd != 10 && cd == ccd) {
                    return true
                }
            }
        }
        return false
    }
}

public struct User {
    let id : Int
    let username : String
    let firstNames : String
    let lastName : String
    let email : String?
    init(id:Int, username:String, firstNames: String, lastName:String, email:String) {
        self.id = id
        self.username = username
        self.firstNames = firstNames
        self.lastName = lastName
        self.email = email
    }
    init(data:NSDictionary) {
        self.id = data.value(forKey: "id") as! Int
        self.username = data.value(forKey: "username") as! String
        self.lastName = data.value(forKey: "lastName") as! String
        self.firstNames = data.value(forKey: "firstNames") as! String
        self.email = data.value(forKey: "email") as? String
    }
    
    var fullName: String {
        get {
            return firstNames + " " + lastName
        }
    }
}

public struct EncounterTemplate {
    let id : Int
    let project : Project
    init(identifier: Int, project:Project) {
        self.id = identifier
        self.project = project
    }
    func asDictionary() -> NSDictionary {
        let data = NSMutableDictionary()
        data.setValue(id, forKey: "id")
        return data
    }
}


public struct ConsentItem {
    let id : Int
    let name : String
    let behaviour : String
    let ordering : Int
    let longDescription : String?
    let mandatoryForParticipation : Bool
    
    var isParticipateItem: Bool {
        get {
            return self.behaviour == "PARTICIPATE"
        }
    }
}

public enum LeafletSectionType {
    
}


public struct LeafletSection {
    let title : String
    let summary : String?
    let content : String?
    let type : String?
    
    init(data:NSDictionary) {
        if let title = data.value(forKey: "title") as? String {
            self.title = title
        }
        else {
            self.title = "No title"
        }
        self.summary = data.value(forKey: "summary") as? String
        self.content = data.value(forKey: "content") as? String
        self.type = data.value(forKey: "type") as? String
    }
}

public struct Leaflet {
    let sections : [LeafletSection]
    init(data: NSArray) {
        var sections = [LeafletSection]()
        for d in data {
            let section = LeafletSection(data: d as! NSDictionary)
            sections.append(section)
        }
        self.sections = sections
    }
}

public struct ConsentForm {
    let id : Int
    let name : String
    let moreInformation : String?
    let hasPdfInformationLeaflet : Bool
    let status : String
    let versionString : String
    let consentItems : [ConsentItem]
    let participateItem : ConsentItem?
    let informationLeaflet : Leaflet?
    var logo : Data?
    init(data:NSDictionary) {
        self.id = data.value(forKey: "id") as! Int
        self.name = data.value(forKey: "name") as! String
        self.moreInformation = data.value(forKey: "moreInformation") as? String
        self.status = data.value(forKey: "status") as! String
        if let _ = data.value(forKey: "informationLeafletAttachment") as? NSDictionary {
            self.hasPdfInformationLeaflet = true
        }
        else {
            self.hasPdfInformationLeaflet = false
        }
        
        self.versionString = data.value(forKey: "versionString") as! String
        var informationLeaflet : Leaflet? = nil
        if let leaflet = data.value(forKey: "informationLeaflet") as? String {
            var format = PropertyListSerialization.PropertyListFormat.xml
            let opts = PropertyListSerialization.ReadOptions(rawValue: PropertyListSerialization.MutabilityOptions().rawValue)
            do {
                let plist = try PropertyListSerialization.propertyList(from: leaflet.data(using: String.Encoding.utf8)!,
                                                                       options:opts, format: &format)
                informationLeaflet = Leaflet(data: plist as! NSArray)
            }
            catch {
            }
        }
        self.informationLeaflet = informationLeaflet
        var myItems = [ConsentItem]()
        let items = data.value(forKey: "consentItems") as! NSArray
        let descriptor: NSSortDescriptor = NSSortDescriptor(key: "ordering", ascending: true)
        let sortedItems: [Any] = items.sortedArray(using: [descriptor])
        var participateItem : ConsentItem?
        for item in sortedItems {
            let i = item as! NSDictionary
            let behaviour = i.value(forKey: "behaviour") as! String
            let ci = ConsentItem(
                id: i.value(forKey: "id") as! Int,
                name: i.value(forKey: "name") as! String,
                behaviour: behaviour,
                ordering: i.value(forKey: "ordering") as! Int,
                longDescription: i.value(forKey: "longDescription") as? String,
                mandatoryForParticipation: i.value(forKey: "mandatoryForParticipation") as! Bool)
            if ci.isParticipateItem {
                participateItem = ci
            }
            myItems.append(ci)
        }
        self.participateItem = participateItem
        self.consentItems = myItems
    }
}

public struct Project {
    let id : Int
    let title : String
    init(identifier:Int, title:String) {
        self.id = identifier
        self.title = title
    }
    init(data: NSDictionary) {
        self.init(identifier:data.value(forKey: "id") as! Int,
                  title:data.value(forKey: "title") as! String)
    }
}

public enum ResearchConsent: String {
    case AGREE = "AGREE"
    case DISAGREE = "DISAGREE"
    case UNDECIDED = "UNDECIDED"
    case NOT_ASKED = "NOT_ASKED"
    
    var description : String {
        get {
            switch(self) {
            case .AGREE:
                return "Yes, I'm interested"
            case .DISAGREE:
                return "No, I don't wish to be involved in research"
            case .UNDECIDED:
                return "I'm not sure"
            case .NOT_ASKED:
                return "Not asked"
            }
        }
    }
    
    static var choices : [ResearchConsent] {
        return [AGREE, DISAGREE, UNDECIDED]
    }
}

public enum Sex : String {
    case male = "MALE"
    case female = "FEMALE"
    case unknown = "UNKNOWN"
    
    static let allValues = [male, female, unknown]
}

public struct Patient {
    let id : Int
    let patientIdentifier : Int
    let firstNames : String
    let sex : Sex
    let lastName : String
    let title : String
    var nhsNumber : String?
    var email : String?
    var alerts : String?
    var currentAddress : Address?
    var referredOrRegisteredProjects : [Project]
    var episodes : [Episode]
    var interestedInResearch : ResearchConsent?
    public init(data:NSDictionary) {
        debugPrint(data)
        self.id = data.value(forKey: "id") as! Int
        self.patientIdentifier = data.value(forKey: "patientIdentifier") as! Int
        self.firstNames = data.value(forKey: "firstNames") as! String
        debugPrint(data.value(forKey: "sex"))
        self.sex = Sex(rawValue:data.value(forKey: "sex") as! String)!
        self.lastName = data.value(forKey: "lastName") as! String
        self.title = data.value(forKey: "title") as! String
        if let nnn = data.value(forKey: "nhsNumber") as? String {
            self.nhsNumber = nnn
        }
        if let email = data.value(forKey: "email") as? String {
            self.email = email
        }
        if let alerts = data.value(forKey: "alerts") as? String {
            self.alerts = alerts
        }
        if let currentAddress = data.value(forKey: "currentAddress") as? NSDictionary {
            self.currentAddress = Address(id: currentAddress.value(forKey: "id") as! Int,
                                          address1: currentAddress.value(forKey: "address1") as? String,
                                          address2: currentAddress.value(forKey: "address2") as? String,
                                          address3: currentAddress.value(forKey: "address3") as? String,
                                          address4: currentAddress.value(forKey: "address4") as? String,
                                          postcode: currentAddress.value(forKey: "postcode") as? String)
        }
        if let researchInterested = data.value(forKey: "interestedInResearch") as? String {
            self.interestedInResearch = ResearchConsent(rawValue: researchInterested)
        }
        var registeredProjects = [Project]()
        if let projects = data.value(forKey: "referredOrRegisteredProjects") as? NSArray {
            for project in projects {
                if let dict = project as? NSDictionary {
                    registeredProjects.append(Project(data: dict))
                }
            }
        }
        self.referredOrRegisteredProjects = registeredProjects
        var patientEpisodes = [Episode]()
        if let episodes = data.value(forKey: "episodes") as? NSArray {
            for episode in episodes {
                if let dict = episode as? NSDictionary {
                    patientEpisodes.append(Episode(data: dict))
                }
            }
        }
        self.episodes = patientEpisodes
    }
    
    var fullName: String {
        get {
            return firstNames + " " + lastName
        }
    }
    
    func isRegisteredOrReferredToProject(_ project:Project) -> Bool {
        for p in referredOrRegisteredProjects {
            if p.id == project.id {
                return true
            }
        }
        return false
    }
}

public struct Episode {
    let id : Int
    let dateDischarge : Date?
    let dateReferral : Date?
    let dateRegistration : Date?
    let project : Project
    let pseudonym : String
    
    init(id:Int, dateReferral:Date?, dateRegistration:Date?, dateDischarge:Date?, project:Project, pseudonym:String) {
        self.id = id
        self.dateReferral = dateReferral
        self.dateRegistration = dateRegistration
        self.dateDischarge = dateDischarge
        self.project = project
        self.pseudonym = pseudonym
    }
    
    init(data: NSDictionary) {
        self.init(
            id:data.value(forKey: "id") as! Int,
            dateReferral: nil,
            dateRegistration: nil,
            dateDischarge: nil,
            project: Project(data:data.value(forKey:"project") as! NSDictionary),
            pseudonym: data.value(forKey: "pseudonym") as! String
        )
    }
    
}

public struct Address {
    let id : Int
    let address1 : String?
    let address2 : String?
    let address3 : String?
    let address4 : String?
    let postcode : String?
}


public struct Encounter {
    var id : Int?
    let patient : Patient
    var durationMinutes : Int?
    var notes : String?
    let encounterTemplate : EncounterTemplate
    let user : User?
    
    init(patient:Patient, encounterTemplate:EncounterTemplate, user:User?) {
        self.id = nil       // has not been saved
        self.patient = patient
        self.encounterTemplate = encounterTemplate
        self.user = user
    }
    
    init(patient:Patient, encounterTemplate:EncounterTemplate, user:User?, data:NSDictionary) {
        self.init(patient: patient, encounterTemplate:encounterTemplate, user:user)
        self.id = data.value(forKey: "id") as? Int
    }
    
    func asDictionary() -> NSDictionary {
        let data = NSMutableDictionary()
        data.setValue(encounterTemplate.asDictionary(), forKey: "encounterTemplate")
        if durationMinutes != nil {
            data.setValue(durationMinutes, forKey: "durationMinutes")
        }
        if notes != nil {
            data.setValue(notes, forKey: "notes")
        }
        if user != nil {
            let users = NSMutableArray()
            let user = NSMutableDictionary()
            user.setValue(self.user!.id, forKey: "id")
            users.add(user)
            data.setValue(users, forKey: "users")
        }
        return data
    }
}


/// A stub patient
public struct StubPatient {
    let data : NSDictionary
    let firstNames : String
    let lastName : String
    let title : String
    //let sex : Sex?
    let hospitalIdentifier : String
    var nhsNumber : String?
    var address1 : String?
    var address2 : String?
    var address3 : String?
    var address4 : String?
    var postcode : String?
    var patient : Patient?
    
    init(data:NSDictionary) {
        self.data = data
        self.firstNames = data.value(forKey: "firstNames") as! String
        self.lastName = data.value(forKey: "lastName") as! String
       // self.sex = Sex.init(rawValue: (data.value(forKey: "sex") as? String)!)
        if let nnn = data.value(forKey: "nhsNumber") as? String {
            self.nhsNumber = nnn
        }
        self.hospitalIdentifier = data.value(forKey: "hospitalIdentifier") as! String
        self.title = data.value(forKey: "title") as! String
        if let a1 = data.value(forKey: "currentAddress1") as? String {
            self.address1 = a1
        }
        if let a2 = data.value(forKey: "currentAddress2") as? String {
            self.address2 = a2
        }
        if let a3 = data.value(forKey: "currentAddress3") as? String {
            self.address3 = a3
        }
        if let a4 = data.value(forKey: "currentAddress4") as? String {
            self.address4 = a4
        }
        if let pc = data.value(forKey: "currentPostcode") as? String {
            self.postcode = pc
        }
    }
    
    var fullName : String {
        get {
            return title + " " + firstNames + " " + lastName
        }
    }
    
    var fullAddress : String {
        get {
            let address = [ address1, address2, address3, address4, postcode ].filter{ $0 != nil}.map{ $0! }
            return address.joined(separator: " ")
        }
    }
}

func < (lhs: StubPatient, rhs: StubPatient) -> Bool {
    return lhs.fullName < rhs.fullName
}

func == (lhs: StubPatient, rhs: StubPatient) -> Bool {
    return lhs.fullName == rhs.fullName
}
