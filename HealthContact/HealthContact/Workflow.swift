//
//  Workflow.swift
//  PatientContactParkinsons
//
//  Created by Mark Wardle on 07/08/2015.
//  Copyright © 2015 Eldrix. All rights reserved.
//
/*
import Foundation

/// A workflow defines the series of steps required in a specific version of the application
protocol Workflow {
    var service : RsdbService { get }
    var encounterTemplate : EncounterTemplate { get }
    
    /// fetch the forms for this defined workflow.
    /// this returns data on the main thread as conceivably, some workflows
    /// will need to hit the network to determine which forms to show
    func fetchForms(patient:Patient, onCompletion: @escaping ((FormSet) -> Void))
}






public class BasicWorkflow {

    var forms : [Form] = []
    func fetchForms(patient: Patient, onCompletion: @escaping ((FormSet) -> Void)) {
        onCompletion(FormSet(forms: forms))
    }
}


public class CardiffNeuroinflammatory : BasicWorkflow {

    let encounterTemplate: EncounterTemplate = EncounterTemplate(identifier: 13,
                                                                 project:Project(identifier:5, title:"Cardiff Neuro-inflammatory service"))  // MS clinic

            FormEdss(),
            FormMsis29()
        ]
        }
}
 */
