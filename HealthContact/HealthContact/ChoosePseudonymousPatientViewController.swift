//
//  ChoosePseudonymousPatient.swift
//  PatientContactParkinsons
//
//  Created by Mark Wardle on 09/12/2016.
//  Copyright © 2016 Eldrix. All rights reserved.
//

import Foundation
import UIKit

class ChoosePseudonymPatientViewController: UIViewController {
    var service : RsdbService?
    var project : Project?
    var completion : ((Patient) -> Void)?
    
    @IBOutlet weak var nhsNumberTextField: UITextField!
    @IBOutlet weak var dateBirthDatePicker: UIDatePicker!
    @IBOutlet weak var sexPicker: UISegmentedControl!
    @IBOutlet weak var errorLabel: UILabel!
    
    let nextButton = UIBarButtonItem(title: "Next", style: .plain, target: self, action: #selector(startQuestionnairesAction))
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        var dateComponents = DateComponents()
        dateComponents.year = 1965
        dateComponents.month = 2
        dateComponents.day = 26
        dateComponents.hour = 12
        dateComponents.minute = 0
        let userCalendar = Calendar.current
        if let initialBirthDate = userCalendar.date(from: dateComponents) {
            dateBirthDatePicker.date = initialBirthDate
        }
        dateBirthDatePicker.maximumDate = Date()
        sexPicker.selectedSegmentIndex = 1
        nextButton.isEnabled = false
        showError(nil)
    }
    
    @IBAction func nhsNumberTextFieldEditingChanged(_ sender: Any) {
        if let nnn = nhsNumberTextField.text {
            if NhsNumber.validateNhsNumber(nnn) {
                nextButton.isEnabled = true
                return
            }
        }
        nextButton.isEnabled = false
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if nhsNumberTextField.text == nil || nhsNumberTextField.text!.count == 0 {
            nhsNumberTextField.becomeFirstResponder()
        }
        self.navigationItem.rightBarButtonItem = nextButton
    }
    
    
    @IBAction func startQuestionnairesAction(_ sender: Any) {
        guard let nnn = nhsNumberTextField.text, nnn.count > 9 else {
            showError("Invalid NHS number")
            return
        }
        guard let service = service else {
            fatalError("No configured RSDB service")
        }
        guard let project = project else {
            fatalError("No project specified")
        }
        let processingAlert = Utility.createBusyAlertWithTitle("Checking patient details", message: "Connecting to server")
        self.present(processingAlert, animated: true, completion: nil)
        let sex = sexPicker.selectedSegmentIndex == 0 ? Sex.male : Sex.female
        service.registerPatientUsingPseudonymToProject(project, nhsNumber: nnn, dateBirth:dateBirthDatePicker.date, sex: sex, registerIfNotFound: true) {
            result in
            switch result {
            case .error(let error):
                print(error)
                let err = (error as NSError).domain
                self.dismiss(animated: false) {
                    let errorAlert = UIAlertController(title: "Error", message: err, preferredStyle: .alert)
                    errorAlert.addAction(UIAlertAction(title: "Continue", style: .default, handler: {
                        (action) in
                        self.dismiss(animated: true)
                    }))
                    self.present(errorAlert, animated: false)
                }
            case .success(let patient):
                print("starting questionnaires for patient: \(patient.patientIdentifier)")
                self.dismiss(animated: false) {
                    if let completion = self.completion {
                        completion(patient)
                    } else {
                        fatalError("No completion handler specified")
                    }
                }
            }
        }
    }
    
    func showError(_ error:String?) {
        if let err = error {
            errorLabel.text = err
            errorLabel.isHidden = false
        } else {
            errorLabel.isHidden = true
        }
    }
}
