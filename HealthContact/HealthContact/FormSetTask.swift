//
//  FormSetTask.swift
//  PatientContactParkinsons
//
//  Created by Mark Wardle on 09/09/2015.
//  Copyright © 2015 Eldrix. All rights reserved.
//

import Foundation
import ResearchKit


/**
A set of forms creating a dynamic task for the user.
A form is a logical set of steps containing at least one step, but perhaps many more.
A form controls navigation within its own steps and so this task delegates to the form first.
However, if a form navigation returns nil, then the task may then move on to the first step
of the next form. Optionally however, depending on the results so far, a form may not be shown.
*/
open class FormSetTask : NSObject, ORKTask {
    @objc public let identifier : String
    let forms : [Form]
    var steps = [ORKStep]()
    var formLookup = [String: Form]()
    var stepLookup = [String: ORKStep]()
    var formForStep = [String: Form]()
    var navigationDelegate : FormSetNavigationDelegate
    var history = [ORKStep]()
    init(identifier: String, forms:[Form]) {
        self.identifier = identifier
        self.forms = forms
        for form in forms {
            let entityName = type(of: form).entityName
            formLookup[entityName] = form
            for step in form.formSteps {
                if stepLookup[step.identifier] != nil {
                    print("Warning: duplicate identifier: \(step.identifier)")
                }
                else {
                    steps.append(step)
                    stepLookup[step.identifier] = step
                    formForStep[step.identifier] = form
                }
            }
        }
        self.navigationDelegate = OrderedFormSetNavigation(forms:forms)
    }
    /// return the form to show after the one specified
    /// this delegates to our navigation delegate
    open func formAfterForm(_ form:Form?, withResult result: ORKTaskResult) -> Form? {
        return self.navigationDelegate.formAfterForm(form, withResults: result)
    }
    
    /**
    Determine the next step. In most cases, we delegate this to the form itself
    but we do have to handle the transition between forms.
    */
    open func step(after step: ORKStep?, with result: ORKTaskResult) -> ORKStep? {
        print("Currently on step: \(String(describing: step))... calculating next step...")
        var nextStep : ORKStep?
        if step == nil {
            if forms.count > 0 {
                nextStep = forms[0].stepAfterStep(step, withResult: result)
            }
        } else {
            if let form = formForStep[step!.identifier] {
                nextStep = form.stepAfterStep(step, withResult: result)
                if nextStep == nil {
                    if let nextForm = self.navigationDelegate.formAfterForm(form, withResults: result) {
                        nextStep = nextForm.stepAfterStep(nil, withResult: result)
                    }
                }
                
            }
        }
        print("Next step is \(String(describing: nextStep))")
        return nextStep
    }
    
    open func step(before step: ORKStep?, with result: ORKTaskResult) -> ORKStep? {
        if step == nil {
            return nil
        }
        if let form = formForStep[step!.identifier] {
            return form.stepBeforeStep(step!, withResult: result)
        }
        return nil
    }
    
    open func step(withIdentifier identifier: String) -> ORKStep? {
        return stepLookup[identifier]
    }
}

public protocol FormSetNavigationDelegate {
    func formAfterForm(_ form:Form?, withResults result:ORKTaskResult) -> Form?
}

public protocol FormNavigationDelegate {
    func stepAfterStep(_ step: ORKStep?, withResult result: ORKTaskResult) -> ORKStep?
    func stepBeforeStep(_ step: ORKStep?, withResult result: ORKTaskResult) -> ORKStep?
}

class OrderedFormNavigationDelegate : FormNavigationDelegate {
    let formSteps : [ORKStep]
    
    init(formSteps:[ORKStep]) {
        self.formSteps = formSteps
    }
    
    /// find the index of the step given
    /// note that this is essentially a swift version of the method from ORKOrderedTask
    func indexOfStep(_ step:ORKStep) -> Int? {
        var index = formSteps.firstIndex(of: step)
        if index == nil {
            for (i, s) in formSteps.enumerated() {
                if s.identifier == step.identifier {
                    index = i
                    break
                }
            }
        }
        return index;
    }
    
    /// find the next step after the step given
    /// this is a swift version of the method from ORKOrderedTask
    func stepAfterStep(_ step: ORKStep?, withResult:ORKTaskResult) -> ORKStep? {
        if formSteps.count <= 0 {
            return nil;
        }
        var nextStep : ORKStep?
        if step == nil {
            nextStep = self.formSteps[0];
        } else {
            let index = indexOfStep(step!)
            if index != nil && index != (formSteps.count-1) {
                nextStep = self.formSteps[index!+1];
            }
        }
        return nextStep;
    }
    
    /// find the prior step before the step given
    //// this is a swift version of the method from ORKOrderedTask
    func stepBeforeStep(_ step: ORKStep?, withResult result: ORKTaskResult) -> ORKStep? {
        let steps = self.formSteps
        if steps.count <= 0 {
            return nil
        }
        var nextStep : ORKStep?
        if step != nil {
            let index = indexOfStep(step!)
            if index != nil && index != 0 {
                nextStep = steps[index!-1]
            }
        }
        return nextStep
    }
    
    func stepWithIdentifier(_ identifier: String) -> ORKStep? {
        for step in formSteps {
            if step.identifier == identifier {
                return step
            }
        }
        return nil
    }
    
}


/**
A simple ordered form navigation which shows forms in order.
It does add a little logic and asks the form whether they should be displayed
given the results so far. As such, it can probably cope with most use cases.
For more complex logic, a custom delegate could be used.
*/
open class OrderedFormSetNavigation : FormSetNavigationDelegate {
    let forms : [Form]
    init(forms: [Form]) {
        self.forms = forms
    }
    func indexOfForm(_ form: Form?) -> Int? {
        if let form = form {
            for (i,f) in forms.enumerated() {
                if type(of: f).entityName == type(of: form).entityName {
                    return i
                }
            }
        }
        return nil
    }
    open func formAfterForm(_ form: Form?, withResults result: ORKTaskResult) -> Form? {
        var nextForm : Form?
        if form == nil {
            if forms.count > 0 {
                nextForm = forms[0]
            }
        }
        else {
            if let index = indexOfForm(form) {
                if index != (forms.count-1) {
                    return forms[index+1]
                }
            }
        }
        if nextForm != nil {
            if nextForm!.shouldShowFormGivenResults(result) {
                return nextForm
            }
            else {
                return formAfterForm(nextForm, withResults: result)
            }
        }
        return nil
    }
}




