//
//  Forms.swift
//  PatientContactParkinsons
//
//  Created by Mark Wardle on 05/08/2015.
//  Copyright © 2015 Eldrix. All rights reserved.
//

import Foundation
import ResearchKit
fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
    switch (lhs, rhs) {
    case let (l?, r?):
        return l < r
    case (nil, _?):
        return true
    default:
        return false
    }
}

fileprivate func > <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
    switch (lhs, rhs) {
    case let (l?, r?):
        return l > r
    default:
        return rhs < lhs
    }
}



/**
 A form represents a logical collection of data combining
 how the information will be gathered (an array of ORKSteps)
 with navigation rules that apply at runtime as well as
 how to appropriately parse the results into a suitable
 format to send to a remote server.
 */
public protocol Form {
    static var entityName: String { get }
    
    /// formstep(s) necessary to implement this form
    var formSteps: [ORKStep] { get }
    
    /// navigation rules for this form
    func navigationWithinFormSet(_ formSet:FormSet) -> [String: ORKStepNavigationRule]
    
    /// should this form be shown given the results so far?
    func shouldShowFormGivenResults(_ results:ORKTaskResult) -> Bool
    
    /// process the results and return the data.
    func processResults(_ results:FormSetResults) -> [FormResult]
}

/**
 This is a default implementation of a form that shows no steps
 */
extension Form {
    
    /// by default, steps are shown one after the other without any navigation rules
    public func navigationWithinFormSet(_ formSet:FormSet) -> [String: ORKStepNavigationRule] {
        return [String: ORKStepNavigationRule]()
    }
    
    /// by default, all forms are happy to be shown, irrespective of results obtained so far
    public func shouldShowFormGivenResults(_ results:ORKTaskResult) -> Bool {
        return true
    }
    
    /// by default, no processing occurs
    public func processResults(_ results: FormSetResults) -> [FormResult] {
        return []
    }
    
}

extension Form {
    public func stepAfterStep(_ step:ORKStep?, withResult:ORKTaskResult) -> ORKStep? {
        return nil
    }
    public func stepBeforeStep(_ step:ORKStep?, withResult:ORKTaskResult) -> ORKStep? {
        return nil
    }
    
}

/// A collection of forms to be shown in sequence to the user.
open class FormSet {
    var forms = [Form]()
    var steps = [ORKStep]()
    var navigation = [String: ORKStepNavigationRule]()
    var formLookup = [String: Form]()
    var stepLookup = [String: ORKStep]()
    
    init(forms:[Form], navigation extraNavigation:[String:ORKStepNavigationRule]? = [:]) {
        self.forms = forms
        for form in forms {
            formLookup[type(of: form).entityName] = form
            for step in form.formSteps {
                steps.append(step)
                if stepLookup[step.identifier] != nil {
                    fatalError("Duplicate identifier: \(step.identifier)")
                }
                stepLookup[step.identifier] = step
            }
        }
        for form in forms {
            for (identifier, rule) in form.navigationWithinFormSet(self) {
                navigation[identifier] = rule
            }
        }
        if extraNavigation != nil {
            for (identifier,rule) in extraNavigation! {
                navigation[identifier] = rule
            }
        }
    }
    
    /// For any given form, find the identifier of the first step of the next form
    func identifierOfFirstStepOfNextForm(_ form:Form) -> String {
        print("Finding next step after form: \(type(of: form).entityName)")
        let lastStep = form.formSteps.last
        if lastStep != nil {
            let index = steps.firstIndex(of: lastStep!)
            if index != nil && (index! + 1) < steps.count {
                let result = steps[index! + 1].identifier
                print("identifier: \(result)")
                return result
            }
        }
        return ""
    }
}

/**
 The results of a FormSet collecting into one convenient package
 with some helper methods to calculate total duration and find
 results matching a specific identifier. This means that our
 individual forms can share common functionality.
 */
open class FormSetResults {
    fileprivate var results = [String: ORKResult]()
    var dateStarted : Date?
    var dateEnded : Date?
    
    var totalDurationSeconds : Double? {
        get {
            if dateStarted != nil {
                return (dateEnded?.timeIntervalSince(dateStarted!))
            }
            return nil
        }
    }
    
    /**
     initialiser that takes a single result from ResearchKit and
     prepares the results
     */
    init(result: ORKResult) {
        processResult(result)
    }
    
    func processResult(_ result:ORKResult) {
        print("Processing result with identifier: \(result.identifier)")
        results[result.identifier] = result
        let start = result.startDate
        let end = result.endDate
        if dateStarted == nil || dateStarted!.compare(start) == .orderedDescending {
            dateStarted = start
        }
        if dateEnded == nil || self.dateEnded!.compare(end) == .orderedAscending {
            dateEnded = end
        }
        
        if let collection = result as? ORKCollectionResult {
            if let steps = collection.results {
                for step in steps {
                    processResult(step)
                }
            }
        }
    }
    
    func resultForIdentifier(_ identifier:String) -> ORKResult? {
        return self.results[identifier]
    }
    
    func booleanResultForIdentifier(_ identifier: String) -> ORKBooleanQuestionResult? {
        return resultForIdentifier(identifier) as? ORKBooleanQuestionResult
    }
    
    func choiceResultForIdentifier(_ identifier: String) -> ORKChoiceQuestionResult? {
        return resultForIdentifier(identifier) as? ORKChoiceQuestionResult
    }
    
    func timeOfDayResultForIdentifier(_ identifier:String) -> ORKTimeOfDayQuestionResult? {
        return resultForIdentifier(identifier) as? ORKTimeOfDayQuestionResult
    }
}


/**
 The result of a form encapsulated ready for transmission
 to an external server.
 It is expected that most logical "forms" will end up as a single FormResult as the
 logical collection of data maps directly to what is considered a form on the server.
 
 However, there may be forms that update multiple entities on the server at the same time.
 For instance, signalling that the patient is interested in research might update a per-encounter
 form but also the main patient record on the server.
 */
public struct FormResult {
    var entityName : String
    var data : NSDictionary
}


struct BooleanFormItem {
    let identifier : String
    let text : String
}

/**
 A form to seek consent from the patient, with information leaflets and consent process
 configured at runtime from information downloaded from the server.
 
 This avoids hard-coding consent into the application and allows the application to show
 new versions of the consent without updating.
 */
class FormConsent : Form {
    static let entityName = "FormConsent"
    let patient : Patient
    let document : ORKConsentDocument
    let consentForm : ConsentForm
    
    let typeMap : [String : ORKConsentSectionType] = [
        "Overview" : .overview,
        "DataGathering" : .dataGathering,
        "Privacy" : .privacy,
        "DataUse" : .dataUse,
        "StudySurvey" : .studySurvey,
        "Withdrawing" : .withdrawing,
        ]
    
    init(forPatient:Patient, withConsent: ConsentForm) {
        self.patient = forPatient
        self.consentForm = withConsent
        let doc = ORKConsentDocument()
        var sections = [ORKConsentSection]()
        if let leaflet = withConsent.informationLeaflet {
            for section in leaflet.sections {
                var type : ORKConsentSectionType? = .custom
                if let sectionType = section.type {
                    if let mappedType = typeMap[sectionType] {
                        type = mappedType
                    }
                }
                let consentSection = ORKConsentSection(type: type!)
                consentSection.summary = section.summary
                consentSection.htmlContent = section.content
                consentSection.title = section.title
                sections.append(consentSection)
            }
            doc.sections = sections
        }
        self.document = doc
    }
    
    lazy var formSteps: [ORKStep] = {
        var steps = [ORKStep]()
        
    
        // ask, are you interested?
        let interested = ORKQuestionStep(identifier: "interested", title: self.consentForm.name, answer: ORKAnswerFormat.booleanAnswerFormat())
        interested.text = self.consentForm.moreInformation
        interested.isOptional = false
        steps.append(interested)
        
        // show the information leaflet
        let stepShow = ORKVisualConsentStep(identifier: "showInformation", document: self.document)
        stepShow.isOptional = true
        steps.append(stepShow)
        
        // go through consent items one-by-one
        // we hide the participate step as that will be checked by the consent step
        var agreeParticipationItem : ConsentItem? = self.consentForm.participateItem
        for consentItem in self.consentForm.consentItems {
            if !consentItem.isParticipateItem {
                let qstep = ORKQuestionStep(identifier: consentItem.name, title: consentItem.name, answer: ORKAnswerFormat.booleanAnswerFormat())
                qstep.text = consentItem.longDescription
                qstep.isOptional = true
                steps.append(qstep)
            }
        }
        
        // ask the consent
        let signature = ORKConsentSignature(forPersonWithTitle: self.patient.fullName, dateFormatString: nil, identifier: "consentSignature")
        signature.requiresName = false
        let stepConsent = ORKConsentReviewStep(identifier: "consent", signature: signature, in: self.document)
        stepConsent.title = "Do you agree to take part?"
        stepConsent.isOptional = true
        if let agreeItem = agreeParticipationItem {
            stepConsent.reasonForConsent = agreeItem.longDescription
        }
        else {
            stepConsent.reasonForConsent = "I would like to take part"
        }
        steps.append(stepConsent)
        
        return steps
    }()
    
    func navigationWithinFormSet(_ formSet:FormSet) -> [String: ORKStepNavigationRule] {
        let isInterested = ORKResultSelector(resultIdentifier: "interested")
        let pIsInterested = ORKResultPredicate.predicateForBooleanQuestionResult(with: isInterested, expectedAnswer: true)
        let rIsInterested = ORKPredicateStepNavigationRule(resultPredicates: [pIsInterested], destinationStepIdentifiers: ["showInformation"], defaultStepIdentifier: formSet.identifierOfFirstStepOfNextForm(self), validateArrays:true)
        
        return ["interested": rIsInterested]
    }
    
    func processResults(_ results:FormSetResults) -> [FormResult] {
        return []
    }
}

/** 
 9-hole peg test
 */
class FormHolePeg : Form {
    static var entityName = "FormHolePeg"
    
    lazy var formSteps : [ORKStep] = {
        let step = ORKHolePegTestPlaceStep(identifier: FormHolePeg.entityName)
        step.numberOfPegs = 9
        step.stepDuration = 120
        return [ step ]
    }()
    
    func processResults(_ results: FormSetResults) -> [FormResult] {
        return []
    }
}

/**
 The PSAT tool.
 */
class FormPsat : Form {
    static var entityName = "FormPsat"
    
    lazy var formSteps : [ORKStep] = {
        let task = ORKOrderedTask.psatTask(withIdentifier: type(of: self).entityName, intendedUseDescription: "Instructions", presentationMode: ORKPSATPresentationMode.auditory.union(.visual), interStimulusInterval: 3.0, stimulusDuration: 1.0, seriesLength: 60, options: [])
        var steps = [ORKStep]()
        for step in task.steps {
            step.isOptional = true
            steps.append(step)
        }
        return steps
    }()
    
    func processResults(_ results:FormSetResults) -> [FormResult] {
        if let result = results.resultForIdentifier(type(of: self).entityName) as? ORKPSATResult {
            print("PSAT result: \(result.totalCorrect)/\(result.length)")
        }
        return []
    }
    
}

class FormSmoking : Form {
    static var entityName = "FormSmoking"
    let smokingChoices = [
        ORKTextChoice(text: "1-5 per day", value: 0 as NSCoding & NSCopying & NSObjectProtocol),
        ORKTextChoice(text: "6-10 per day", value: 1 as NSCoding & NSCopying & NSObjectProtocol),
        ORKTextChoice(text: "11-20 per day",value: 2 as NSCoding & NSCopying & NSObjectProtocol),
        ORKTextChoice(text: "21-30 per day", value:3 as NSCoding & NSCopying & NSObjectProtocol),
        ORKTextChoice(text: "31-40 per day", value:4 as NSCoding & NSCopying & NSObjectProtocol),
        ORKTextChoice(text: "more than 40 per day", value:5 as NSCoding & NSCopying & NSObjectProtocol),
        ]
    
    
    /// formstep(s) necessary to implement this form
    var formSteps: [ORKStep] {
        let doYouSmoke = ORKQuestionStep(identifier: "doYouSmoke", title: "Do you smoke?", answer: ORKAnswerFormat.booleanAnswerFormat())
        let numberSmoked = ORKQuestionStep(identifier:"numberSmoked", title:"How much do you typically smoke in a day?", answer: ORKAnswerFormat.choiceAnswerFormat(with: ORKChoiceAnswerStyle.singleChoice, textChoices: self.smokingChoices))
        let interestedInGivingUp = ORKQuestionStep(identifier: "interestedInGivingUp", title: "Are you interested in giving up smoking cigarettes?", answer: ORKAnswerFormat.booleanAnswerFormat())
        let exampleIntervention = ORKInstructionStep(identifier: "smokingIntervention")
        exampleIntervention.title = "Giving up smoking"
        exampleIntervention.text = "As you live in ..., then you can access the smoking cessation clinic on... "
        return [doYouSmoke, numberSmoked, interestedInGivingUp, exampleIntervention]
    }
    
    /// navigation rules for this form
    func navigationWithinFormSet(_ formSet:FormSet) -> [String: ORKStepNavigationRule] {
        var rules = [String: ORKStepNavigationRule]()
        let nextFormIdentifier = formSet.identifierOfFirstStepOfNextForm(self)
        
        let patientSmokes = ORKResultSelector(resultIdentifier: "doYouSmoke")
        let pPatientDoesSmoke = ORKResultPredicate.predicateForBooleanQuestionResult(with: patientSmokes, expectedAnswer: true)
        let pPatientDoesNotSmoke = ORKResultPredicate.predicateForBooleanQuestionResult(with: patientSmokes, expectedAnswer: false)
        rules["doYouSmoke"] = ORKPredicateStepNavigationRule(resultPredicates: [pPatientDoesSmoke, pPatientDoesNotSmoke], destinationStepIdentifiers: ["numberSmoked",nextFormIdentifier], defaultStepIdentifier: nextFormIdentifier, validateArrays:true)
        return rules
    }
    
    /// should this form be shown given the results so far?
    func shouldShowFormGivenResults(_ results:ORKTaskResult) -> Bool {
        return true
    }
    
    /// process the results and return the data.
    func processResults(_ results:FormSetResults) -> [FormResult] {
        return []
    }
    
}


/**
 The EQ5D form.
 */
class FormEq5d : Form {
    static var entityName = "FormEq5d"
    let startingVas : Int
    
    //
    // create a new EQ5D with an optional starting value
    // if a starting value is not provided, then a random choice between (0,50,100) will be made.
    init(startingVas:Int? = nil) {
        if startingVas != nil {
            self.startingVas = startingVas!
        }
        else {
            let random = arc4random_uniform(2)
            self.startingVas = Int(random) * 50
        }
    }
    
    let mobilityChoices = [
        ORKTextChoice(text: "I have no problems walking about", value: 0 as NSCoding & NSCopying & NSObjectProtocol),
        ORKTextChoice(text: "I have some problems in walking about", value:1 as NSCoding & NSCopying & NSObjectProtocol),
        ORKTextChoice(text: "I am confined to bed", value: 2 as NSCoding & NSCopying & NSObjectProtocol),
        ]
    
    let selfCareChoices = [
        ORKTextChoice(text: "I have no problems with self-care", value: 0 as NSCoding & NSCopying & NSObjectProtocol),
        ORKTextChoice(text: "I have some problems washing or dressing myself", value:1 as NSCoding & NSCopying & NSObjectProtocol),
        ORKTextChoice(text: "I am unable to wash or dress myself", value: 2 as NSCoding & NSCopying & NSObjectProtocol),
        ]
    let usualActivitiesChoices = [
        ORKTextChoice(text: "I have no problems with performing my usual activities", value: 0 as NSCoding & NSCopying & NSObjectProtocol),
        ORKTextChoice(text: "I have some problems with performing my usual activities", value:1 as NSCoding & NSCopying & NSObjectProtocol),
        ORKTextChoice(text: "I am unable to perform my usual activities", value: 2 as NSCoding & NSCopying & NSObjectProtocol),
        ]
    let painDiscomfortChoices = [
        ORKTextChoice(text: "I have no pain or discomfort", value: 0 as NSCoding & NSCopying & NSObjectProtocol),
        ORKTextChoice(text: "I have moderate pain or discomfort", value:1 as NSCoding & NSCopying & NSObjectProtocol),
        ORKTextChoice(text: "I have extreme pain or discomfort", value: 2 as NSCoding & NSCopying & NSObjectProtocol),
        ]
    let anxietyDepressionChoices = [
        ORKTextChoice(text: "I am not anxious or depressed", value: 0 as NSCoding & NSCopying & NSObjectProtocol),
        ORKTextChoice(text: "I am moderately anxious or depressed", value:1 as NSCoding & NSCopying & NSObjectProtocol),
        ORKTextChoice(text: "I am extremely anxious or depressed", value: 2 as NSCoding & NSCopying & NSObjectProtocol),
        ]
    
    lazy var formSteps : [ORKStep] = {
        let eq5d = ORKFormStep(identifier: "FormEq5d", title: "Your health", text: "Which statements best describe your own health state today?")
        let mobility = ORKFormItem(identifier: "mobility", text: "Mobility", answerFormat: ORKAnswerFormat.choiceAnswerFormat(with: ORKChoiceAnswerStyle.singleChoice, textChoices: self.mobilityChoices))
        let selfCare = ORKFormItem(identifier: "selfCare", text: "Self-care", answerFormat: ORKAnswerFormat.choiceAnswerFormat(with: .singleChoice, textChoices: self.selfCareChoices))
        let usualActivities = ORKFormItem(identifier: "usualActivities", text: "Usual activities", answerFormat: ORKAnswerFormat.choiceAnswerFormat(with: .singleChoice, textChoices: self.usualActivitiesChoices))
        let painDiscomfort = ORKFormItem(identifier: "painDiscomfort", text: "Pain / discomfort", answerFormat: ORKAnswerFormat.choiceAnswerFormat(with: .singleChoice, textChoices: self.painDiscomfortChoices))
        let anxietyDepression = ORKFormItem(identifier: "anxietyDepression", text: "Anxiety / depression", answerFormat: ORKAnswerFormat.choiceAnswerFormat(with: .singleChoice, textChoices: self.anxietyDepressionChoices))
        eq5d.formItems = [mobility, selfCare, usualActivities, painDiscomfort, anxietyDepression]
        let vas = ORKQuestionStep(identifier: "FormEq5d.vas", title: "Your own health state today", answer: ORKAnswerFormat.continuousScale(withMaximumValue: 100, minimumValue: 0, defaultValue: Double(self.startingVas), maximumFractionDigits: 0, vertical: true, maximumValueDescription: "Best imaginable health state", minimumValueDescription: "Worst imaginable health state"))
        vas.text = "We would like you to indicate on this scale how good or bad your own health is today, in your opinion. Drag the slider to whichever point on the scale indicates how good or bad your health state is today."
        return [eq5d, vas]
    }()
    
    func processResults(_ results:FormSetResults) -> [FormResult] {
        if let stepResult = results.resultForIdentifier("FormEq5d") as? ORKStepResult {
            if let vasResult = results.resultForIdentifier("FormEq5d.vas") as? ORKScaleQuestionResult {
                if let results = stepResult.results {
                    let json = NSMutableDictionary()
                    json.setValue("FormEq5d", forKey: "type")
                    json.setValue(self.startingVas, forKey:"vasStartingScore")
                    for result in results {
                        var value : AnyObject = NSNull()
                        if let choice = result as? ORKChoiceQuestionResult {
                            if choice.choiceAnswers?.count == 1 {
                                value = choice.choiceAnswers![0] as AnyObject
                            }
                        }
                        json.setValue(value, forKey: result.identifier)
                    }
                    let vas : Int? = (vasResult.scaleAnswer != nil) ? Int(truncating: vasResult.scaleAnswer!) : nil
                    json.setValue(vas, forKey: "visualAnalogueScale")
                    let userData = NSMutableDictionary()
                    userData.setValue(1, forKey: "id")
                    userData.setValue("User", forKey: "type")
                    json.setValue(userData, forKey: "user")
                    return [FormResult(entityName: "FormEq5d", data: json)]
                }
            }
        }
        return []
    }
}

/**
 The EDSS, a commonly used multiple sclerosis outcome measurement, derived from the patient.
 
 This form adapts to each response to minimise asking the patient questions that
 do not aid in calculating the EDSS.
 */
class FormEdss : Form {
    static var entityName = "FormEdss"
    
    enum EdssScore : String {
        case SCORE0_0 = "SCORE0_0"
        case SCORE1_0 = "SCORE1_0"
        case SCORE1_5 = "SCORE1_5"
        case SCORE2_0 = "SCORE2_0"
        case SCORE2_5 = "SCORE2_5"
        case SCORE3_0 = "SCORE3_0"
        case SCORE3_5 = "SCORE3_5"
        case SCORE_LESS_THAN_4 = "SCORE_LESS_THAN_4"
        case SCORE4_0 = "SCORE4_0"
        case SCORE4_5 = "SCORE4_5"
        case SCORE5_0 = "SCORE5_0"
        case SCORE5_5 = "SCORE5_5"
        case SCORE6_0 = "SCORE6_0"
        case SCORE6_5 = "SCORE6_5"
        case SCORE7_0 = "SCORE7_0"
        case SCORE7_5 = "SCORE7_5"
        case SCORE8_0 = "SCORE8_0"
        case SCORE8_5 = "SCORE8_5"
        case SCORE9_0 = "SCORE9_0"
        case SCORE9_5 = "SCORE9_5"
        case SCORE10_0 = "SCORE10_0"
    }
    
    let walkingDistanceChoices = [
        ORKTextChoice(text: "Unrestricted - I can walk for 2-3 hours with no problems", value: 0 as NSCoding & NSCopying & NSObjectProtocol),
        ORKTextChoice(text: "Clearly more than 500 metres / 550 yards", value: 1 as NSCoding & NSCopying & NSObjectProtocol),
        ORKTextChoice(text: "About 500 metres / 550 yards",detailText:"(about the length of Queen St in Cardiff)", value:2 as NSCoding & NSCopying & NSObjectProtocol, exclusive: true),
        ORKTextChoice(text: "About 300 metres / 330 yards", detailText:"(length of 3 football pitches)", value:3 as NSCoding & NSCopying & NSObjectProtocol, exclusive:true),
        ORKTextChoice(text: "About 200 metres / 220 yards", detailText:"(half of Queen St or 2 football pitches)", value:4 as NSCoding & NSCopying & NSObjectProtocol, exclusive:true),
        ORKTextChoice(text: "About 100 metres / 110 yards", detailText:"(length of one football pitch or the outpatients corridor at UHW)", value:5 as NSCoding & NSCopying & NSObjectProtocol, exclusive:true),
        ORKTextChoice(text: "Less than 100 metre / 110 yards",value:6 as NSCoding & NSCopying & NSObjectProtocol)
    ]
    let walkingDescriptionChoices = [
        ORKTextChoice(text: "I can walk more than or about 100 metres / 110 yards (the length of a football pitch) with one stick or person for support", value:0 as NSCoding & NSCopying & NSObjectProtocol),
        ORKTextChoice(text: "I use an FES machine on one leg to walk", value:1 as NSCoding & NSCopying & NSObjectProtocol),
        ORKTextChoice(text: "I use an FES machine on two legs to walk", value:2 as NSCoding & NSCopying & NSObjectProtocol),
        ORKTextChoice(text: "I need support on both sides to walk, such as 2 sticks or a walker or frame",value:3 as NSCoding & NSCopying & NSObjectProtocol),
        ORKTextChoice(text: "I can only walk 10 metres (just about across a room)", value:4 as NSCoding & NSCopying & NSObjectProtocol),
        ORKTextChoice(text: "I can only walk a few steps even with help and normally use a wheelchair", value:5 as NSCoding & NSCopying & NSObjectProtocol),
        ORKTextChoice(text: "I use a wheelchair all of the time and can't even take a few steps", value:6 as NSCoding & NSCopying & NSObjectProtocol)
    ]
    
    lazy var formSteps : [ORKStep] = {
        let canWalkIndependently = ORKQuestionStep(identifier: "canWalkIndependently", title: "Are you able to walk independently?", answer: ORKAnswerFormat.booleanAnswerFormat())
        canWalkIndependently.text = "Tick yes if can walk without the use of a walking stick or frame or person to hold or FES machine or other support."
        
        let walkingDistance = ORKQuestionStep(identifier: "walkingDistance", title: "At your BEST, how far can you walk ?", answer: ORKAnswerFormat.choiceAnswerFormat(with: ORKChoiceAnswerStyle.singleChoice, textChoices: self.walkingDistanceChoices))
        walkingDistance.text = "How far can you walk without a rest or help from a stick or frame or person or FES or other walking aid?"
        
        let walkingDescription = ORKQuestionStep(identifier: "walkingDescription", title: "What is your walking ability?", answer: ORKAnswerFormat.choiceAnswerFormat(with: ORKChoiceAnswerStyle.singleChoice, textChoices: self.walkingDescriptionChoices));
        walkingDescription.text = "Tick which description best describes your walking ability."
        
        let difficultyHands = ORKQuestionStep(identifier: "difficultyHands", title: "Do you have trouble using your hands for writing and eating etc?", answer: ORKAnswerFormat.booleanAnswerFormat())
        
        let totallyConfined = ORKQuestionStep(identifier: "totallyConfined", title: "Are you totally confined to bed and need help with all daily tasks?", answer: ORKAnswerFormat.booleanAnswerFormat())
        return [canWalkIndependently, walkingDistance, walkingDescription, difficultyHands, totallyConfined]
    }()
    
    func navigationWithinFormSet(_ formSet:FormSet) -> [String: ORKStepNavigationRule] {
        var rules = [String: ORKStepNavigationRule]()
        let nextFormIdentifier = formSet.identifierOfFirstStepOfNextForm(self)
        
        let canWalkIndependently = ORKResultSelector(resultIdentifier: "canWalkIndependently")
        let pCanWalk = ORKResultPredicate.predicateForBooleanQuestionResult(with: canWalkIndependently, expectedAnswer: true)
        let pCannotWalk = ORKResultPredicate.predicateForBooleanQuestionResult(with: canWalkIndependently, expectedAnswer: false)
        rules["canWalkIndependently"] = ORKPredicateStepNavigationRule(resultPredicates: [pCanWalk, pCannotWalk], destinationStepIdentifiers: ["walkingDistance","walkingDescription"], defaultStepIdentifier: nextFormIdentifier, validateArrays:true)
        let walkingDistance = ORKResultSelector(resultIdentifier: "walkingDistance")
        let walkingEnd = ORKResultPredicate.predicateForChoiceQuestionResult(with: walkingDistance, expectedAnswerValues: [0 as NSCoding & NSCopying & NSObjectProtocol,1 as NSCoding & NSCopying & NSObjectProtocol,2 as NSCoding & NSCopying & NSObjectProtocol,3 as NSCoding & NSCopying & NSObjectProtocol,4 as NSCoding & NSCopying & NSObjectProtocol,5 as NSCoding & NSCopying & NSObjectProtocol])
        let walkingMore = ORKResultPredicate.predicateForChoiceQuestionResult(with: walkingDistance, expectedAnswerValue: 6 as NSCoding & NSCopying & NSObjectProtocol)
        rules["walkingDistance"] = ORKPredicateStepNavigationRule(resultPredicates: [walkingEnd, walkingMore], destinationStepIdentifiers: [nextFormIdentifier, "walkingDescription"], defaultStepIdentifier: nextFormIdentifier, validateArrays:true)
        
        
        let walkingDescription = ORKResultSelector(resultIdentifier: "walkingDescription")
        let notUsesWheelchair = ORKResultPredicate.predicateForChoiceQuestionResult(with: walkingDescription, expectedAnswerValues: [0 as NSCoding & NSCopying & NSObjectProtocol,1 as NSCoding & NSCopying & NSObjectProtocol,2 as NSCoding & NSCopying & NSObjectProtocol,3 as NSCoding & NSCopying & NSObjectProtocol,4 as NSCoding & NSCopying & NSObjectProtocol,5 as NSCoding & NSCopying & NSObjectProtocol])
        let usesWheelchair = ORKResultPredicate.predicateForChoiceQuestionResult(with: walkingDescription, expectedAnswerValue: 6 as NSCoding & NSCopying & NSObjectProtocol)
        rules["walkingDescription"] = ORKPredicateStepNavigationRule(resultPredicates: [notUsesWheelchair, usesWheelchair], destinationStepIdentifiers: [nextFormIdentifier, "difficultyHands"], defaultStepIdentifier: nextFormIdentifier, validateArrays:true)
        let difficultyHands = ORKResultSelector(resultIdentifier: "difficultyHands")
        let pDifficultyHands = ORKResultPredicate.predicateForBooleanQuestionResult(with: difficultyHands, expectedAnswer: true)
        let pNoDifficultyHands = ORKResultPredicate.predicateForBooleanQuestionResult(with: difficultyHands, expectedAnswer: false)
        rules["difficultyHands"] = ORKPredicateStepNavigationRule(resultPredicates: [pDifficultyHands, pNoDifficultyHands], destinationStepIdentifiers: ["totallyConfined", nextFormIdentifier], defaultStepIdentifier: nextFormIdentifier, validateArrays:true)
        return rules
    }
    
    func processResults(_ results:FormSetResults) -> [FormResult] {
        //let walkIndependently = results.resultForIdentifier("canWalkIndependently") as? ORKBooleanQuestionResult
        let walkingDistance = results.resultForIdentifier("walkingDistance") as? ORKChoiceQuestionResult
        let walkingDescription = results.resultForIdentifier("walkingDescription") as? ORKChoiceQuestionResult
        let difficultyHands = results.resultForIdentifier("difficultyHands") as? ORKBooleanQuestionResult
        let totallyConfined = results.resultForIdentifier("totallyConfined") as? ORKBooleanQuestionResult
        
        var edss : EdssScore?
        if let isConfined = totallyConfined?.booleanAnswer {
            edss = (isConfined == 1) ? EdssScore.SCORE9_0 : EdssScore.SCORE8_5
        }
        if difficultyHands?.booleanAnswer == false {
            edss = EdssScore.SCORE8_5
        }
        if walkingDescription?.choiceAnswers?.count == 1 {
            let value = walkingDescription!.choiceAnswers![0] as! Int
            if value == 0 || value == 1 {
                edss = EdssScore.SCORE6_0
            } else if value == 2 || value == 3 {
                edss = EdssScore.SCORE6_5
            } else if value == 4 {
                edss = EdssScore.SCORE7_0
            } else if value == 5 {
                edss = EdssScore.SCORE8_0
            }
        }
        if walkingDistance?.choiceAnswers?.count == 1 {
            let value = walkingDistance!.choiceAnswers![0] as! Int
            if value == 0 || value == 1 {
                edss = EdssScore.SCORE_LESS_THAN_4
            } else if value == 2 {
                edss = EdssScore.SCORE4_0
            } else if value == 3 {
                edss = EdssScore.SCORE4_5
            } else if value == 4 {
                edss = EdssScore.SCORE5_0
            } else if value == 5 {
                edss = EdssScore.SCORE5_5
            }
        }
        if edss != nil {
            print("EDSS : \(String(describing: edss))")
            let json = NSMutableDictionary()
            let userData = NSMutableDictionary()
            userData.setValue(1, forKey: "id")
            userData.setValue("User", forKey: "type")
            json.setValue(userData, forKey: "user")
            json.setValue(edss?.rawValue, forKey: "edssScore")
            json.setValue(FormEdss.entityName, forKey: "type")
            return [FormResult(entityName: FormEdss.entityName, data: json)]
        }
        return []
    }
}

/**
 The MSIS-29, a multiple sclerosis impact scale.
 */
class FormMsis29 : Form {
    static var entityName = "FormMsis29"
    
    let choices = [
        ORKTextChoice(text: "Not at all", detailText: nil, value: 1 as NSCoding & NSCopying & NSObjectProtocol, exclusive: true),
        ORKTextChoice(text: "A little", detailText: nil, value: 2 as NSCoding & NSCopying & NSObjectProtocol, exclusive: true),
        ORKTextChoice(text: "Moderately", detailText: nil, value: 3 as NSCoding & NSCopying & NSObjectProtocol, exclusive: true),
        ORKTextChoice(text: "Quite a bit", detailText: nil, value: 4 as NSCoding & NSCopying & NSObjectProtocol, exclusive: true),
        ORKTextChoice(text: "Extremely", detailText: nil, value: 5 as NSCoding & NSCopying & NSObjectProtocol, exclusive: true),
        ]
    
    lazy var formSteps : [ORKStep] = {
        //let answerFormat = ORKScaleAnswerFormat(maximumValue: 5, minimumValue: 1, defaultValue: 3, step: 1, vertical: false, maximumValueDescription: "Extremely", minimumValueDescription: "Not at all")
        let answerFormat = ORKTextChoiceAnswerFormat(style: .singleChoice, textChoices: self.choices)
        
        let msForm = ORKFormStep(identifier: FormMsis29.entityName, title: "The impact of multiple sclerosis", text: "The following questions ask for your views about the impact of MS on your day-to-day life during the past two weeks.")
        
        msForm.formItems = [
            ORKFormItem(sectionTitle: "In the past two weeks, how much has your MS limited your ability to...?"),
            ORKFormItem(identifier: "doPhysicallyDemandingTasks", text: "...do physically demanding tasks?", answerFormat: answerFormat),
            ORKFormItem(identifier: "gripThingsTightly", text: "...grip things tightly e.g. turning on taps?", answerFormat: answerFormat),
            ORKFormItem(identifier: "carryThings", text: "...carry things?", answerFormat: answerFormat),
            
            ORKFormItem(sectionTitle: "In the past two weeks, how much have you been bothered by...?"),
            ORKFormItem(identifier: "problemsWithBalance", text: "...problems with your balance?", answerFormat: answerFormat),
            ORKFormItem(identifier: "difficultiesMovingIndoors", text: "...difficulties moving about indoors?", answerFormat: answerFormat),
            ORKFormItem(identifier: "beingClumsy", text: "...being clumsy?", answerFormat: answerFormat),
            ORKFormItem(identifier: "stiffness", text: "...stiffness?", answerFormat: answerFormat),
            ORKFormItem(identifier: "heavyArmsOrLegs", text: "...heavy arms and/or legs?", answerFormat: answerFormat),
            ORKFormItem(identifier: "tremorArmsOrLegs", text: "...tremor of your arms or legs?", answerFormat: answerFormat),
            ORKFormItem(identifier: "spasmsLimbs", text: "...spasms in your limbs?", answerFormat: answerFormat),
            ORKFormItem(identifier: "bodyNotDoingWhatWant", text: "...your body not doing what you want it to do?", answerFormat: answerFormat),
            ORKFormItem(identifier: "dependOnOthersToDoThings", text: "...having to depend on others to do things for you?", answerFormat: answerFormat),
            ORKFormItem(identifier: "limitationsSocialLeisure", text: "...limitations in your social and leisure activities at home?", answerFormat: answerFormat),
            ORKFormItem(identifier: "beingStuckAtHome", text: "Being stuck at home more than you would like to be?", answerFormat: answerFormat),
            ORKFormItem(identifier: "difficultiesUsingHandsEverydayTasks", text: "Difficulties using your hands in everyday tasks?", answerFormat: answerFormat),
            ORKFormItem(identifier: "cutDownWorkOrOtherActivities", text: "Having to cut down the amount of time you spent on work or other daily activities?", answerFormat: answerFormat),
            ORKFormItem(identifier: "problemsUsingTransport", text: "Problems using transport (e.g. car, bus, train, taxi, etc.)?", answerFormat: answerFormat),
            ORKFormItem(identifier: "takingLonger", text: "Taking longer to do things?", answerFormat: answerFormat),
            ORKFormItem(identifier: "difficultySpontaneity", text: "Difficulty doing things spontaneously (e.g. going out on the spur of the moment)?", answerFormat: answerFormat),
            ORKFormItem(identifier: "needingToiletUrgently", text: "Needing to go to the toilet urgently?", answerFormat: answerFormat),
            ORKFormItem(identifier: "feelingUnwell", text: "Feeling unwell?", answerFormat: answerFormat),
            ORKFormItem(identifier: "problemsSleeping", text: "Problems sleeping?", answerFormat: answerFormat),
            ORKFormItem(identifier: "feelingMentallyFatigued", text: "Feeling mentally fatigued?", answerFormat: answerFormat),
            ORKFormItem(identifier: "worriesRelatingToCondition", text: "Worries related to your MS?", answerFormat: answerFormat),
            ORKFormItem(identifier: "anxiousOrTense", text: "Feeling anxious or tense?", answerFormat: answerFormat),
            ORKFormItem(identifier: "irritableImpatientShortTempered", text: "Feeling irritable, impatient, or short tempered?", answerFormat: answerFormat),
            ORKFormItem(identifier: "problemsConcentrating", text: "Problems concentrating?", answerFormat: answerFormat),
            ORKFormItem(identifier: "lackOfConfidence", text: "Lack of confidence?", answerFormat: answerFormat),
            ORKFormItem(identifier: "feelingDepressed", text: "Feeling depressed?", answerFormat: answerFormat),
        ]
        return [msForm]
    }()
    
    func processResults(_ results:FormSetResults) -> [FormResult] {
        if let formResult = results.resultForIdentifier(type(of: self).entityName) as? ORKStepResult {
            let data = NSMutableDictionary()
            data.setValue(type(of: self).entityName, forKey: "type")
            let userData = NSMutableDictionary()
            userData.setValue(1, forKey: "id")
            userData.setValue("User", forKey: "type")
            data.setValue(userData, forKey: "user")
            for result in formResult.results! {
                if let r = result as? ORKChoiceQuestionResult {
                    if let chosen = r.choiceAnswers {
                        if chosen.count == 1 {
                            let answer = chosen.first! as! Int
                            data.setValue(answer, forKey: r.identifier)
                        }
                    }
                    else {
                        data.setValue(NSNull(), forKey: r.identifier)
                    }
                }
            }
            return [FormResult(entityName: type(of: self).entityName, data: data)]
        }
        return [];
    }
}

/// A simple form just to tell the user that they've finished.
class FormCompleted : Form {
    static var entityName = "FormProm"
    lazy var formSteps : [ORKStep] = {
        let completionStep = ORKCompletionStep(identifier: "completed")
        completionStep.text = "You have completed this questionnaire. Please hand the device back to a member of staff."
        completionStep.title = "Thank you"
        return [completionStep]
    }()
}

class FormLogin : Form {
    static var entityName = "FormLogin"
    let title : String
    let text : String
    let loginViewControllerClass : AnyClass
    
    init(title:String, text:String, loginViewControllerClass:AnyClass) {
        self.title = title
        self.text = text
        self.loginViewControllerClass = loginViewControllerClass
    }
    
    lazy var formSteps : [ORKStep] = {
        let step = ORKLoginStep(identifier: "login", title: self.title, text:self.text, loginViewControllerClass:self.loginViewControllerClass)
        return [step, ]
    }()
}

/**
 A generic way of asking the patient whether they are interested in taking part
 in research.
 */
class FormResearchInterested : Form {
    static var entityName = "FormResearch"
    let title : String
    init(title:String = "Finally, are you interested in taking part in clinical research?") {
        self.title = title
    }
    
    lazy var formSteps : [ORKStep] = {
        var values = [ORKTextChoice]()
        for consent in ResearchConsent.choices {
            values.append(ORKTextChoice(text: consent.description, value:consent.rawValue as NSCoding & NSCopying & NSObjectProtocol))
        }
        let research = ORKQuestionStep(identifier: "researchInterested", title: self.title, answer: ORKAnswerFormat.choiceAnswerFormat(with: .singleChoice, textChoices: values))
        research.text = "If you agree, we will flag your records so that you can be contacted for future research projects."
        return [research, ]
    }()
    
    func processResults(_ results:FormSetResults) -> [FormResult] {
        let data = NSMutableDictionary()
        if let result = results.choiceResultForIdentifier("researchInterested") {
            let chosen = result.choiceAnswers
            var answer = ResearchConsent.UNDECIDED
            if chosen?.count > 0 {
                if let choice = chosen?.first as? String {       // should be the VALUE of the textchoice chosen
                    if let choice2 = ResearchConsent(rawValue: choice) {
                        answer = choice2
                    }
                }
            }
            data.setValue(answer.rawValue, forKey: "interestedInResearch")
        }
        return [FormResult(entityName: "Patient", data: data)]
    }
}


/**
 A tapping active task which is useful for patients with, for example, Parkinson's disease
 as a measure of bradykinesia.
 */
class FormTapping: Form {
    static var entityName = "FormTapping"
    
    lazy var formSteps : [ORKStep] = {
        let bundle = Bundle(for: ORKInstructionStep.self)
        // instruction page1
        let instruction = ORKInstructionStep(identifier: "tappingInstruction")
        instruction.title = "Tapping Speed"
        instruction.text = "We are interested to know the speed of your movements and would like you to do a finger tapping test."
        instruction.detailText = "This activity measures your tapping speed."
        
        let imageName = "phonetapping";
        instruction.image = UIImage(named: imageName, in:bundle, compatibleWith:nil)
        //instruction.shouldTintImages = true;
        
        let takenDrugs = ORKQuestionStep(identifier: "tappingHasTaken", title: "Have you taken any treatment for Parkinson's today?", answer: ORKAnswerFormat.booleanAnswerFormat())
        let drugTiming = ORKQuestionStep(identifier: "tappingDrugTiming", title: "What time did you last take any treatment for Parkinson's disease?" , answer: ORKTimeOfDayAnswerFormat())
        drugTiming.text = "If you are currently taking treatment (e.g. an apomorphine pump) then leave the time as the current time. If you have not taken any treatment today then please skip this question."
        
        
        // instruction page2
        let instruction2 = ORKInstructionStep(identifier: "tappingInstruction2")
        instruction2.title = "Tapping Speed"
        instruction2.text = "Rest the device on a flat surface. Two buttons will appear on your screen for 10 seconds. Using two fingers on the same hand, take turns tapping the buttons as quickly as you can."
        instruction2.detailText = "Tap 'Next' to begin."
        let im1 = UIImage(named: "handtapping01", in:bundle, compatibleWith:nil)
        let im2 = UIImage(named: "handtapping02", in:bundle, compatibleWith:nil)
        //UIImage *im1 = [UIImage imageNamed:@"handtapping01" inBundle:[NSBundle bundleForClass:[self class]] compatibleWithTraitCollection:nil];
        //UIImage *im2 = [UIImage imageNamed:@"handtapping02" inBundle:[NSBundle bundleForClass:[self class]] compatibleWithTraitCollection:nil];
        instruction2.image = UIImage.animatedImage(with: [im1!, im2!], duration: 1)
        //instruction2.shouldTintImages = true;
        
        // the actual step
        /*        NSMutableArray *recorderConfigurations = [NSMutableArray arrayWithCapacity:5];
         if (! (ORKPredefinedTaskOptionExcludeAccelerometer & options)) {
         [recorderConfigurations addObject:[[ORKAccelerometerRecorderConfiguration alloc] initWithIdentifier:ORKAccelerometerRecorderIdentifier
         frequency:100]];
         }
         */
        let tapping = ORKTappingIntervalStep(identifier:"tapping")
        tapping.title = "Tap the buttons as quickly as you can using two fingers."
        tapping.stepDuration = 10
        tapping.shouldContinueOnFinish = true
        tapping.isOptional = true
        //step.recorderConfigurations = recorderConfigurations;
        return [instruction, drugTiming, instruction2, tapping]
    }()
    
    func processResults(_ results:FormSetResults) -> [FormResult] {
        if let result = results.resultForIdentifier("tapping") as? ORKTappingIntervalResult,
            let samples = result.samples {
            let data = NSMutableDictionary()
            let numberOfTaps = samples.count
            data.setValue(numberOfTaps, forKey: "numberOfTaps")
            data.setValue(10, forKey: "durationSeconds")
            
            return [FormResult(entityName: type(of: self).entityName, data: data)]
        }
        return []
    }
}

/**
 A non-motor symptom screening questionnaire asking about non-motor symtoms in Parkinson's disease.
 */
class FormParkinsonNonMotor: Form {
    static var entityName = "FormParkinsonNonMotor"
    
    let useMultipleChoice : Bool;
    init(useMultipleChoice:Bool = false) {
        self.useMultipleChoice = useMultipleChoice
    }
    
    let pdNonMotorSx = [
        BooleanFormItem(identifier: "dribblingSalivaDaytime", text: "Dribbling of saliva during the daytime"),
        BooleanFormItem(identifier: "lossChangeTasteSmell", text: "Loss or change in your ability to taste or smell"),
        BooleanFormItem(identifier: "difficultySwallowing", text: "Difficulty swallowing food or drink or problems with choking"),
        BooleanFormItem(identifier: "vomitingNausea", text: "Vomiting or feelings of sickness (nausea)."),
        BooleanFormItem(identifier: "constipation", text: "Constipation (less than three bowel movements a week) or having to strain to pass a stool."),
        BooleanFormItem(identifier: "faecalIncontinence", text: "Bowel (faecal) incontinence."),
        BooleanFormItem(identifier: "incompleteBowelEmptying", text: "Feeling that your bowel emptying is incomplete after having been to the toilet."),
        BooleanFormItem(identifier: "urinaryUrgency", text: "A sense of urgency to pass urine makes you rush to the toilet."),
        BooleanFormItem(identifier: "nightPassingUrine", text: "Getting up regularly at night to pass urine."),
        BooleanFormItem(identifier: "unexplainedPains", text: "Unexplained pains (not due to known conditions such as arthritis)."),
        BooleanFormItem(identifier: "unexplainedChangeWeight", text: "Unexplained change in weight (not due to change in diet)."),
        BooleanFormItem(identifier: "problemsRemembering", text: "Problems remembering things that have happened recently or forgetting to do things."),
        BooleanFormItem(identifier: "lossInterest", text: "Loss of interest in what is happening around you or in doing things."),
        BooleanFormItem(identifier: "hallucinations", text: "Seeing or hearing things that you know or are told are not there."),
        BooleanFormItem(identifier: "difficultyConcentrating", text: "Difficulty concentrating or staying focused."),
        BooleanFormItem(identifier: "feelingSad", text: "Feeling sad, ‘low’ or ‘blue’."),
        BooleanFormItem(identifier: "feelingAnxious", text: "Feeling anxious, frightened or panicky."),
        BooleanFormItem(identifier: "lessOrMoreInterestedInSex", text: "Feeling less interested in sex or more interested in sex."),
        BooleanFormItem(identifier: "difficultyHavingSexWhenTry", text: "Finding it difficult to have sex when you try."),
        BooleanFormItem(identifier: "lightheadedDizzy", text: "Feeling light-headed, dizzy or weak standing from sitting or lying."),
        BooleanFormItem(identifier: "falling", text: "Falling."),
        BooleanFormItem(identifier: "difficultyStayingAwake", text: "Finding it difficult to stay awake during activities such as working, driving or eating."),
        BooleanFormItem(identifier: "difficultGettingSleep", text: "Difficulty getting to sleep at night or staying asleep at night."),
        BooleanFormItem(identifier: "intenseVividFrighteningDreams", text: "Intense, vivid or frightening dreams."),
        BooleanFormItem(identifier: "talkingMovingInSleep", text: "Talking or moving about in your sleep, as if you are ‘acting out’ a dream."),
        BooleanFormItem(identifier: "unpleasantSensationsLegs", text: "Unpleasant sensations in your legs at night or while resting, and a feeling that you need to move."),
        BooleanFormItem(identifier: "legSwelling", text: "Swelling of the legs."),
        BooleanFormItem(identifier: "excessiveSweating", text: "Excessive sweating."),
        BooleanFormItem(identifier: "doubleVision", text: "Double vision."),
        BooleanFormItem(identifier: "delusions", text: "Believing things are happening to you that other people say are not."),
        ]
    
    func booleanFormAsTextChoices(_ formItems: [BooleanFormItem]) -> [ORKTextChoice] {
        var choices : [ORKTextChoice] = [ORKTextChoice]()
        for formItem in formItems {
            choices.append(ORKTextChoice(text: formItem.text, value: formItem.identifier as NSCoding & NSCopying & NSObjectProtocol))
        }
        return choices
    }
    
    func booleanFormAsFormItems(_ formItems: [BooleanFormItem]) -> [ORKFormItem] {
        var choices : [ORKFormItem] = [ORKFormItem]()
        let format = ORKBooleanAnswerFormat()
        for formItem in formItems {
            choices.append(ORKFormItem(identifier: formItem.identifier, text: formItem.text, answerFormat: format))
        }
        return choices
    }
    
    lazy var formSteps: [ORKStep] = {
        let pdNonMotorSymptoms = ORKTextChoiceAnswerFormat(style: .multipleChoice, textChoices: self.booleanFormAsTextChoices(self.pdNonMotorSx))
        
        let pdNmsForm = ORKFormStep(identifier:"FormParkinsonNonMotor",
                                    title:"Non-motor symptoms of Parkinson's disease",
                                    text:"The movement symptoms of Parkinson's are well known. However, other problems can sometimes occur as part of the condition or its treatment. It is important that the doctor knows about these, particularly if they are troublesome for you.\n\nA range of problems is listed below. Please choose 'YES' if you have experienced it during the past month. You should answer 'NO' even if you have had the problem in the past but not in the past month.")
        pdNmsForm.isOptional = true
        if self.useMultipleChoice {
            pdNmsForm.formItems = [
                ORKFormItem(identifier: "pdNonMotor", text:"What problems have you had in the last month?", answerFormat:pdNonMotorSymptoms),
            ]
        }
        else {
            pdNmsForm.formItems = self.booleanFormAsFormItems(self.pdNonMotorSx)
        }
        return [pdNmsForm]
    }()
    
    func processResults(_ results:FormSetResults) -> [FormResult] {
        if let result = results.resultForIdentifier("FormParkinsonNonMotor") {
            return [FormResult(entityName: type(of: self).entityName, data: _processForBoolean(result))]
        }
        return []
    }
    
    fileprivate func _processForBoolean(_ result: ORKResult) -> NSDictionary {
        assert(result.identifier == type(of: self).entityName)
        print("Processing Parkinson's non motor form")
        let json = NSMutableDictionary()
        json.setValue(type(of: self).entityName, forKey: "type")
        let userData = NSMutableDictionary()
        userData.setValue(1, forKey: "id")
        userData.setValue("User", forKey: "type")
        json.setValue(userData, forKey: "user")
        
        if let stepResult = result as? ORKStepResult {
            if let results = stepResult.results {
                for result in results {
                    var value : AnyObject = NSNull()
                    if let booleanResult = result as? ORKBooleanQuestionResult {
                        if let answer = booleanResult.booleanAnswer {
                            value = answer.stringValue as AnyObject
                        }
                    }
                    json.setValue(value, forKey: result.identifier)
                }
            }
        }
        return json
    }
}
