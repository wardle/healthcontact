//
//  PatientConfirmViewController.swift
//  PatientContactParkinsons
//
//  Created by Mark Wardle on 01/08/2015.
//  Copyright © 2015 Eldrix. All rights reserved.
//

import Foundation
import UIKit
import ResearchKit

class PatientConfirmViewController: UITableViewController {
    @IBOutlet var patientDetailsTableView: UITableView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var address1Label: UILabel!
    @IBOutlet weak var address2Label: UILabel!
    @IBOutlet weak var address3Label: UILabel!
    @IBOutlet weak var address4Label: UILabel!
    @IBOutlet weak var postcodeLabel: UILabel!
    
    @IBOutlet weak var detailsCorrectCell: UITableViewCell!
    @IBOutlet weak var detailsIncorrectCell: UITableViewCell!
    
    // inputs
    var selectedPatient : StubPatient?
    var service : RsdbService?
    var encounterTemplate : EncounterTemplate?
    var completion : ((Patient) -> Void)?

    // results
    var patient : Patient?
    
    override func viewWillAppear(_ animated:Bool) {
        super.viewWillAppear(animated)
        guard let selectedPatient = selectedPatient else {
            fatalError("no selected patient")
        }
        guard let encounterTemplate = encounterTemplate else {
            fatalError("no encounter template")
        }
        if patient == nil {
            let pleaseWait = Utility.createBusyAlertWithTitle("Loading", message:"Fetching patient record...")
            self.present(pleaseWait, animated: true) {
                
                self.service?.findOrRegisterPatient(selectedPatient, forProject: encounterTemplate.project, andEncounterTemplate: encounterTemplate) {
                    result in
                    switch result {
                    case .success(let pt):
                        self.nameLabel.text = pt.fullName
                        self.address1Label.text = pt.currentAddress?.address1
                        self.address2Label.text = pt.currentAddress?.address2
                        self.address3Label.text = pt.currentAddress?.address3
                        self.address4Label.text = pt.currentAddress?.address4
                        self.postcodeLabel.text = pt.currentAddress?.postcode
                        self.patient = pt
                        self.patientDetailsTableView.reloadData()
                        self.dismiss(animated: true, completion: nil)
                    case .error(let error):
                        self.dismiss(animated: true) {
                            let alert = UIAlertController(title: "There was a problem fetching patient details",
                                                          message: error.localizedDescription,
                                                          preferredStyle: UIAlertController.Style.alert)
                            let continueAction = UIAlertAction(title: "Continue", style: UIAlertAction.Style.default) {
                                alertAction in
                                self.backToLoginScreen()
                            }
                            alert.addAction(continueAction)
                            self.present(alert, animated: true, completion: nil)
                        }
                    }
                }
            }
        }
    }
    
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        debugPrint("selected \((indexPath as NSIndexPath).section)-\((indexPath as NSIndexPath).row)")
        switch ((indexPath as NSIndexPath).section, (indexPath as NSIndexPath).row) {
        case (1,0):
            debugPrint("selected yes")
            self.dismiss(animated: false) {
                if let completion = self.completion {
                    completion(self.patient!)
                } else {
                    fatalError("No completion handler specified")
                }
            }
        case (1,1):
            let alert = UIAlertController(
                title:"An error with your details",
                message:"Thank you for letting us know your details are incorrect. Please hand the device back to a member of staff. They may update your NHS details and try again later.",
                preferredStyle: UIAlertController.Style.alert)
            let continueAction = UIAlertAction(title: "Continue", style: UIAlertAction.Style.default) {
                (UIAlertAction) -> Void in
                self.backToLoginScreen()
            }
            alert.addAction(continueAction)
            self.present(alert, animated: true, completion: nil)
        default:
            return;
        }
    }
    
    
    func backToLoginScreen()  {
        var mainNavigation : UINavigationController? = self.navigationController?.presentingViewController as? UINavigationController
        if mainNavigation == nil {
            mainNavigation = self.navigationController
        }
        mainNavigation?.dismiss(animated: false) {
            _ = mainNavigation?.popToRootViewController(animated: true)
        }
    }
    
}
