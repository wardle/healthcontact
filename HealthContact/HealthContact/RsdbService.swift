//
//  RsdbClient.swift
//  PatientContact
//
//  Created by Mark Wardle on 22/06/2014.
//  Copyright (c) 2014-2016 Eldrix. All rights reserved.
//

import Foundation

/// Returns a result or an error
///
/// - Success: The object expected
/// - Error: An error
public enum Result<T, ErrorType> {
    case success(T)
    case error(Error)
    
    /// a default error
    static var defaultError : Error {
        return NSError(domain:"Unknown error", code:-1, userInfo:nil)
    }
    
    /// Convenience method to create wrap an object and an error into a result.
    ///
    /// - Parameters:
    ///   - t: the object to return
    ///   - e: the error
    /// - Returns: <#return value description#>
    public static func result(_ t:T?, _ e:Error?) -> Result {
        if e == nil && t != nil {
            return .success(t!)
        }
        return .error(e ?? defaultError)
    }
    
    public static func withResult(_ t:T?) -> Result {
        return Result.result(t, nil)
    }
    
    public static func withError(_ e:Error?) -> Result {
        return Result.result(nil, e)
    }
    
    /// Convenience tester/getter for the value
    public var result: T? {
        switch self {
        case .success(let v): return v
        case .error: return nil
        }
    }
    
    /// Convenience tester/getter for the error
    public var error: Error? {
        switch self {
        case .success: return nil
        case .error(let e): return e
        }
    }
    
    /// Test whether the result is an error.
    public var isError: Bool {
        switch self {
        case .success: return false
        case .error: return true
        }
    }
    
    public var isSuccess: Bool {
        return !isError
    }
}



enum RsdbError : Error {
    case error(title:String, debug:String?)

    case invalidPatientDetails(String?)      // user has passed in invalid patient details
    case general(error: Error?)
    case jsonMapping
    case jsonSerialization
    
}

public protocol RsdbService {
    
    ///  Does this service need to use pseudonyms or not?
    ///
    var usePseudonyms: Bool { get }
    
    
    ///  Perform a login action using the specified credentials.
    ///
    ///   - parameter username:       username of the user
    ///   - parameter password:       password of the user
    ///   - parameter onCompletion:   completion handler to be called on main thread
    ///   - parameter result:         the user or an error
    /// - Returns: nothing, as the result will be returned in the callback
    func loginWithUsername(_ username: String, password:String, onCompletion: @escaping(_ result:Result<User, Error>) -> Void)
    
    /// Fetch a list of users matching the specific query
    ///
    /// - Parameters:
    ///   - query: a dictionary for the query
    ///   - onCompletion: completion handler, to be called on main thread
    ///   - result: the list of users or an error
    /// - Returns: nothing
    func fetchUsersWithQuery(_ query:[String:String], onCompletion:@escaping(_ result:Result<[User], Error>) -> Void)
    
    /// Fetch a list of patients for a particular encounter template
    ///
    /// This will return the appointments for that day, usually from a patient administration system (PAS).
    /// It returns a stub patient record, a minimal set of data allowing the list to be shown to clinical users
    /// but for subsequent calls, will need to be inflated to a full patient record by formally registering the patient
    ///
    /// - Parameters:
    ///   - project: Project
    ///   - et: the encounter template for the project
    ///   - date: date of the clinical event
    ///   - onCompletion: completion handler, to be called on main thread
    ///   - result: the stub patient or an error
    /// - Returns: <#return value description#>
    func fetchPatientsForProject(_ project:Project, et:EncounterTemplate, onDate date:Date,
                                 onCompletion:@escaping(_ result:Result<[StubPatient], Error>) -> Void)
    
    /// Fetch a patient via a pseudonym, optionally registering a new patient if no patient found
    ///
    /// Patients have a global pseudonym based on NHS number and a project-specific pseudonym for
    /// each project to which they are registered.
    ///
    /// This method will register a patient if not already registered and the NHS number is valid, according
    /// to the server.
    ///
    /// - Parameters:
    ///   - project: the project to which the patient should be registered
    ///   - nhsNumber: NHS number of the patient
    ///   - dateBirth: date of birth
    ///   - sex: sex
    ///   - registerIfNotFound: should these details be used to register a patient if no matching patient found?
    ///   - onCompletion: completion handler to be called, on the main thread
    ///   - result: the patient or an error
    /// - Returns: nothing, as the result will be returned in the callback
    func registerPatientUsingPseudonymToProject(_ project:Project, nhsNumber:String, dateBirth:Date, sex:Sex, registerIfNotFound:Bool, onCompletion:@escaping(_ result:Result<Patient, Error>) ->Void)
    
    /// Fetch a patient via a pseudonym
    /// - Parameters
    ///   - project: the project to which the patient should be registered
    ///   - pseudonym: some characters from the pseudonym.
    ///   - onCompletion: completion handler to be called, on the main thread
    ///   - result: the patient or an error
    /// - Returns: nothing, as the result will be returned in the callback
    func fetchPatientWithPseudonym(_ pseudonym:String, forProject project:Project, onCompletion:@escaping(_ result:Result<Patient, Error>)->Void)
    
    /// Get a list of the projects suitable for this patient
    ///
    /// - Parameters:
    ///   - patient: the patient
    ///   - onCompletion: completion handler to be called on main thread
    ///   - result: the list of projects or an error
    /// - Returns: nothing, as the result will be returned in the callback
    func fetchResearchForPatient(_ patient: Patient, onCompletion: @escaping (_ result:Result<[Project], Error>) -> Void)
    
    /// Fetch consent form for the given project with the specified identifier.
    ///
    /// - Parameters:
    ///   - projectId: project identifier
    ///   - consentFormId: consent form identifier
    ///   - onCompletion: completion handler to be called on main thread
    ///   - result: the consent form or an error
    /// - Returns: nothing, as the result will be returned in the callback
    func fetchConsentForm(_ projectId:Int, consentFormId:Int, completion onCompletion: @escaping (_ result:Result<ConsentForm, Error>) -> Void)
    
    /// Fetch a consent form information leaflet from the remote server
    ///
    /// - Parameters:
    ///   - projectId: project identifier
    ///   - consentFormId: consent form identifier
    ///   - onCompletion: completion handler to be called on main thread
    ///   - result: data or an error
    /// - Returns: nothing, as the result will be returned in the callback
    func fetchConsentFormInformationLeaflet(_ projectId:Int, consentFormId:Int, onCompletion:@escaping(_ result:Result<Data, Error>) -> Void)
    
    /// Fetch the logo for the specified project
    ///
    /// - Parameters:
    ///   - projectId: project identifier
    ///   - onCompletion: completion handler to be called on main thread
    ///   - result: data or an error
    /// - Returns: nothing, as the result will be returned in the callback
    func fetchLogoForProjectWithProjectIdentifier(_ projectId:Int, onCompletion:@escaping(_ result:Result<Data, Error>) -> Void)
    
    /// Find or create a new registration for the given patient
    ///
    /// - Parameters:
    ///   - patient: patient to be found or registered
    ///   - project: project
    ///   - encounterTemplate: encounter template for the type of encounter
    ///   - onCompletion: completion handler to be called on main thread
    /// - Returns: nothing, as the result will be returned in the callback
    func findOrRegisterPatient(_ patient: StubPatient, forProject project: Project, andEncounterTemplate encounterTemplate: EncounterTemplate, onCompletion:@escaping(_ result:Result<Patient, Error>) -> Void)
    
    /// Create an encounter on the remote server.
    ///
    /// - Parameters:
    ///   - encounter: the encounter to be created
    ///   - onCompletion: completion handler to be called on main thread
    ///   - result: the created encounter or an error
    func createEncounter(_ encounter:Encounter, onCompletion: @escaping(_ result:Result<Encounter, Error>) -> Void)
    
    /// Update the patient using the data specified
    ///
    /// - Parameters:
    ///   - patient: the patient
    ///   - data: data
    func updatePatient(_ patient:Patient, data:Data)
    
    /// Publish a set of forms with the specified results for the specified encounter
    ///
    /// - Parameters:
    ///   - forms: the set of forms
    ///   - results: the results of those forms
    ///   - encounter: the encounter to which the forms should be created
    func publishFormset(_ forms:FormSet, withResults results:FormSetResults, forEncounter encounter:Encounter)
    
    /// Publish form data for the encounter specified.
    ///
    /// - Parameters:
    ///   - data: data for the form
    ///   - encounter: encounter to which the form data should be published
    func publishForm(_ data:Data, forEncounter encounter:Encounter)
    
}

