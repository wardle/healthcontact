//
//  ChoosePatientViewController.swift
//  PatientContactParkinsons
//
//  Created by Mark Wardle on 31/07/2015.
//  Copyright © 2015 Eldrix. All rights reserved.
//

import Foundation
import UIKit

class ChooseListViewController: UITableViewController {
    var service : RsdbService?
    var encounterTemplate : EncounterTemplate?
    var patients  = [StubPatient]()
    var completion : ((Patient) -> Void)?

    var selectedPatient : StubPatient?
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1;
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return patients.count
    }
    
    override func shouldPerformSegue(withIdentifier identifier: String, sender: Any?) -> Bool {
        if identifier == "go_patient" {
            return false
        }
        return true
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        super.prepare(for: segue, sender: sender)
        if let pt = selectedPatient {
            if segue.identifier == "go_patient" {
                // we perform a "show replace" segue rather than a "push" segue.
                // This prevents backtracking, preventing a patient from going back to the
                // list of patients.
                // However, it means we need a new navigation controller so to find our first
                // real view controller, we must fetch it from the navigation controller.
                // we may then pass on our application state.
                let newNavigation = segue.destination as! UINavigationController
                let confirmVc = newNavigation.topViewController as! PatientConfirmViewController
                confirmVc.service = self.service
                confirmVc.selectedPatient = pt
                confirmVc.encounterTemplate = encounterTemplate
                confirmVc.completion = completion
            }
        }
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ChoosePatientCell", for:indexPath) as! PatientCell
        let patient = patients[(indexPath as NSIndexPath).row]
        cell.patientNameLabel.text = patient.firstNames + " " + patient.lastName
        cell.hospitalIdentifierLabel.text = patient.hospitalIdentifier
        cell.addressLabel.text = patient.fullAddress
        cell.nhsNumberLabel.text = patient.nhsNumber
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let patient = patients[(indexPath as NSIndexPath).row]
        print("Selected patient : \(patient.fullName)")
        let alert = UIAlertController(title:"Confirm patient", message: "You are about to create questionnaires for \(patient.fullName)", preferredStyle: UIAlertController.Style.alert)
        let continueAction = UIAlertAction(title: "Continue", style: UIAlertAction.Style.default) {
            (UIAlertAction) -> Void in
            self.selectedPatient = patient
            self.performSegue(withIdentifier: "go_patient", sender: self)
            self.patients = []
            self.tableView.reloadData()
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel, handler: nil)
        alert.addAction(cancelAction)
        alert.addAction(continueAction)
        self.present(alert, animated: true, completion: nil)
    }
}


class PatientCell : UITableViewCell {
    @IBOutlet weak var hospitalIdentifierLabel: UILabel!
    @IBOutlet weak var patientNameLabel: UILabel!
    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var nhsNumberLabel: UILabel!
}
