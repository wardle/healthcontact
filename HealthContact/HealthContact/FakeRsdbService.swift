//
//  FakeRsdbService.swift
//  PatientContactParkinsons
//
//  Created by Mark Wardle on 05/10/2015.
//  Copyright © 2015 Eldrix. All rights reserved.
//

import Foundation

/**
    This is a fake service simply for demonstration purposes.
*/
class FakeRsdbService : RsdbService {

    ///  Does this service need to use pseudonyms or not?
    ///
    var usePseudonyms: Bool = false


    func delay(_ delay:Double, closure:@escaping ()->()) {
        DispatchQueue.main.asyncAfter(
            deadline: DispatchTime.now() + Double(Int64(delay * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC), execute: closure)
    }
    
    /// perform a login action using the specified credentials
    func loginWithUsername(_ username:String, password:String, onCompletion:@escaping(_ result:Result<User, Error>) -> Void) {
        if username == "test" {
            let user = User(id: 1, username: username, firstNames: "Test", lastName: "Test", email: "test@eldrix.co.uk")
            delay(0.5) {
                onCompletion(Result.success(user))
            }
        }
    }
    
    /// fetch a list of users matching the specific query
    func fetchUsersWithQuery(_ query:[String:String], onCompletion:@escaping(Result<[User], Error>) -> Void) {
        onCompletion(Result.withResult([]))
    }
    
    /// fetch a list of patients for a particular encounter template
    func fetchPatientsForProject(_ project:Project, et:EncounterTemplate, onDate date:Date,
        onCompletion:@escaping(Result<[StubPatient], Error>) -> Void) {
            delay(1) {
                onCompletion(Result.success([ StubPatient(data: FakeRsdbService.fakePatient())  ]))
            }
        }
    
    static func fakePatient() -> NSDictionary {
        let data = NSMutableDictionary()
        data.setValue(1, forKey:"patientIdentifier")
        data.setValue(1, forKey:"id")
        data.setValue("John", forKey: "firstNames")
        data.setValue("Smith", forKey: "lastName")
        data.setValue("Mr", forKey: "title")
        data.setValue(Sex.male.rawValue, forKey: "sex")
        data.setValue("a123456", forKey: "hospitalIdentifier")
        data.setValue("123456789", forKey: "nhsNumber")
        data.setValue("1 Station Road", forKey: "currentAddress1")
        data.setValue("Whitchurch", forKey: "currentAddress2")
        data.setValue("Cardiff", forKey: "currentAddress3")
        data.setValue("South Glamorgan", forKey: "currentAddress4")
        data.setValue("CF14 4XW", forKey: "currentPostcode")
        return data
    }
    public func registerPatientUsingPseudonymToProject(_ project: Project, nhsNumber: String, dateBirth: Date, sex: Sex, registerIfNotFound: Bool, onCompletion: @escaping (Result<Patient, Error>) -> Void) {
        onCompletion(Result.success(Patient(data:FakeRsdbService.fakePatient())))
    }
    
    open func fetchPatientWithPseudonym(_ pseudonym:String, forProject project:Project, onCompletion:@escaping(Result<Patient, Error>)->Void) {
        if pseudonym.count >= 3 {
            onCompletion(Result.withResult(Patient(data:FakeRsdbService.fakePatient())))
        }
        onCompletion(Result.withError(NSError(domain: "Not found", code: 404, userInfo: nil)))
    }

    
    /// Get a list of the projects suitable for this patient
    public func fetchResearchForPatient(_ patient: Patient, onCompletion:@escaping(Result<[Project], Error>) -> Void) {
        delay(1) {
            onCompletion(Result.success([]))
        }
    }
    
    /// Fetch consent form for the given project with the specified identifier.
    public func fetchConsentForm(_ projectId:Int, consentFormId:Int, completion onCompletion:@escaping(Result<ConsentForm, Error>) -> Void) {
        delay(1) {
            let data = NSMutableDictionary()
            data.setValue(1, forKey: "id")
            data.setValue("ConsentForm", forKey: "type")
            //data.setValue(<#T##value: Any?##Any?#>, forKey: <#T##String#>)
            //let consentForm = ConsentForm(data: data)
            onCompletion(Result.withError(NSError(domain: "Not implemented", code: 0, userInfo: nil)))
        }
    }
    
    /// Fetch a consent form information leaflet from the remote server
    public func fetchConsentFormInformationLeaflet(_ projectId:Int, consentFormId:Int, onCompletion:@escaping(Result<Data, Error>) -> Void) {
        onCompletion(Result.withError(nil))
    }

    

    
    /// Fetch the logo for the specified project
    public func fetchLogoForProjectWithProjectIdentifier(_ projectId: Int, onCompletion: @escaping (Result<Data, Error>) -> Void) {
        onCompletion(Result.withError(nil))
    }
    
    /// Find or create a new registration for the given patient
    public func findOrRegisterPatient(_ patient: StubPatient, forProject project: Project, andEncounterTemplate encounterTemplate: EncounterTemplate, onCompletion:@escaping (Result<Patient, Error>) -> Void) {
        let data = FakeRsdbService.fakePatient()
        if let address1 = patient.address1 {
            let address = NSMutableDictionary()
            address.setValue(1, forKey: "id")
            address.setValue(address1, forKey: "address1")
            address.setValue(patient.address2, forKey:"address2")
            address.setValue(patient.address3, forKey:"address3")
            address.setValue(patient.address4, forKey:"address4")
            address.setValue(patient.postcode, forKey:"postcode")
            data.setValue(address, forKey: "currentAddress")
        }
        delay(2) {
            onCompletion(Result.success(Patient(data: data)))
        }
    }

    /// Create an encounter on the remote server.
    public func createEncounter(_ encounter:Encounter, onCompletion:@escaping(Result<Encounter, Error>) -> Void) {
        delay(0.5) {
            onCompletion(Result.success(encounter))
        }
    }

    /// Update the patient using the data specified
    public func updatePatient(_ patient:Patient, data:Data) {

    }
    
    /// Publish a set of forms with the specified results for the specified encounter
    public func publishFormset(_ forms:FormSet, withResults results:FormSetResults, forEncounter encounter:Encounter) {
        
    }
    
    /// Publish form data for the encounter specified.
    public func publishForm(_ data:Data, forEncounter encounter:Encounter) {
        
    }
}
