//
//  Utility.swift
//  PatientContactParkinsons
//
//  Created by Mark Wardle on 09/12/2016.
//  Copyright © 2016 Eldrix. All rights reserved.
//

import Foundation
import UIKit

class Utility {
    
    /**
     Create an alert controller designed to show an informative alert
     and activity indicator (spinner) to show that something is happening and the user
     will have to wait until the task is complete.
     */
    class public func createBusyAlertWithTitle(_ title:String, message:String) -> UIAlertController {
        //create an alert controller
        let pending = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        //create an activity indicator
        let indicator = UIActivityIndicatorView(frame: pending.view.bounds)
        indicator.style = .whiteLarge
        indicator.color = UIColor(red: 0, green: 0, blue: 0, alpha: 0.5)
        indicator.translatesAutoresizingMaskIntoConstraints = false
        pending.view.addSubview(indicator)
        
        let views : [String : AnyObject] = ["pending" : pending.view, "indicator" : indicator]
        var constraints =  NSLayoutConstraint.constraints(withVisualFormat: "H:|[indicator]|", options: [], metrics: nil, views: views)
        
        constraints += NSLayoutConstraint.constraints(withVisualFormat: "V:[indicator]-(100)-|", options: [], metrics: nil, views: views)
        pending.view.addConstraints(constraints)
        //add the activity indicator as a subview of the alert controller's view
        pending.view.addSubview(indicator)
        indicator.isUserInteractionEnabled = false // required otherwise if there buttons in the UIAlertController you will not be able to press them
        indicator.startAnimating()
        return pending
    }

   }

/// A simple utility class to create a UIPickerView datasource and delegate from a
/// static array
public class UIPickerViewDataSourceForArray<T> : NSObject, UIPickerViewDataSource, UIPickerViewDelegate {
    
    let data:[T]
    let dataToString:((T) -> String)?
    let didSelectRow:((T) -> Void)?
    public var selected:T?
    
    convenience init(data:[T]) {
        self.init(data: data, dataToString:nil, didSelectRow: nil)
    }
    init(data:[T], dataToString: ((T) -> String)?, didSelectRow: ((T) -> Void)?) {
        self.data = data
        self.dataToString = dataToString
        self.didSelectRow = didSelectRow
    }
    
    public func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    public func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return data.count
    }
    
    public func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        let converter = dataToString ?? { T in String(describing: T).uppercased() }
        return converter(data[row])
    }
    public func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        selected = data[row]
        didSelectRow?(data[row])
    }
}

