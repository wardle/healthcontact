//
//  LoginController.swift
//
//  Created by Mark Wardle on 29/07/2015.
//  Copyright © 2015 Eldrix. All rights reserved.
//

import Foundation
import UIKit

class LoginController: UIViewController, UITextFieldDelegate {
    var service : RsdbService?
    var loginAction : ((User, RsdbService) -> Void)?
    var projectTitle : String?
    
    @IBOutlet weak var usernameTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var loginButton: UIButton!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var projectTitleLabel: UILabel!
    
    @IBAction func performLoginAction(_ sender: AnyObject) {
        if let username = usernameTextField.text, let password = passwordTextField.text {
            debugPrint("Performing login for username: \(username)")
            lockLoginControls(true)
            if username == "test" {
                service = FakeRsdbService()
            }
            guard let service = service else {
                showError("Invalid URL specified for HealthContact service")
                self.lockLoginControls(false)
                return
            }
            service.loginWithUsername(username, password: password) {
                result in
                defer {
                    self.lockLoginControls(false)
                }
                switch result {
                case .error(let err):
                    debugPrint("Error with user login: \(err.localizedDescription)")
                    let errorMessage = "Error: " + (err._code == 401 ? "Invalid username or password" : err.localizedDescription)
                    self.showError(errorMessage)
                case .success(let user):
                    debugPrint("Logged in as user \(user)")
                    if let action = self.loginAction {
                        action(user, service)
                    } else {
                        fatalError("Error: no login action specified")
                    }
                }
            }
        }
    }

    func showError(_ message:String) {
        let alert = UIAlertController(title: "Login error", message: message, preferredStyle: UIAlertController.Style.alert)
        let alertAction = UIAlertAction(title: "Continue", style: UIAlertAction.Style.default) {
            (UIAlertAction) -> Void in
        }
        alert.addAction(alertAction)
        self.present(alert, animated: true)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        usernameTextField.delegate = self
        passwordTextField.delegate = self
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        lockLoginControls(false)
        if usernameTextField.text == nil || usernameTextField.text!.count == 0 {
            usernameTextField.becomeFirstResponder()
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if let projectTitle = projectTitle {
            projectTitleLabel.isHidden = false
            projectTitleLabel.text = projectTitle
        } else {
            projectTitleLabel.isHidden = true
        }
    }
    
    func lockLoginControls(_ lock: Bool) {
        usernameTextField.isEnabled = !lock
        usernameTextField.isEnabled = !lock
        loginButton.isEnabled = !lock
        activityIndicator.isHidden = !lock
        switch lock {
        case true: activityIndicator.startAnimating()
        case false: activityIndicator.stopAnimating()
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == usernameTextField {
            passwordTextField.becomeFirstResponder()
        }
        if textField == passwordTextField {
            performLoginAction(self)
        }
        return true
    }
}
