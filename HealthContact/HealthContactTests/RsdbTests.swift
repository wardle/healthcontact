//
//  HealthContactCoreTests.swift
//  HealthContactCoreTests
//
//  Created by Mark Wardle on 10/12/2016.
//  Copyright © 2016 Eldrix. All rights reserved.
//

import XCTest

class HealthContactCoreTests: XCTestCase {
    
    let service = PatientCareService(baseUrl: URL(string:"http://localhost/cgi-bin/WebObjects/RSNews.woa/-5555/ra/")!)
    
    var formatter : DateFormatter {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy/MM/dd"
        return formatter
    }
    
    override func setUp() {
        super.setUp()
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func _testLogin(username:String, password:String, expectSuccess success:Bool) {
        let expect = expectation(description: "Perform login for username \(username) expected success \(success)")
        service.loginWithUsername(username, password: password) {
            result in
            switch result {
            case .error(let error):
                XCTAssertEqual((error as NSError).code, 401)      // unauthorised
                XCTAssertFalse(success)
            case .success(let user):
                XCTAssertTrue(success)
                XCTAssertEqual(username, user.username)
            }
            expect.fulfill()
        }
        waitForExpectations(timeout: 5) { error in
            if let error = error {
                XCTFail("waitForExpectations errored: \(error)")
            }
        }
    }
    
    func testLogin() {
        _testLogin(username: "system", password: "wibble", expectSuccess:false)
        _testLogin(username: "wibble", password: "wibble", expectSuccess:false)
        _testLogin(username: "system", password: "password", expectSuccess:true)
    }
    
    func _fetchPatientViaPseudonym(project:Project, nhsNumber:String, dateBirth:String, sex:Sex, expectSuccess success:Bool) {
        let expect = expectation(description: "Fetch patient using pseudonymous identifier \(nhsNumber) \(dateBirth) \(sex)")
        service.loginWithUsername("system", password: "password") {
            result in
            switch result {
            case .error(let error):
                XCTFail("Could not login successfully: \(error)")
            case .success(let user):
                XCTAssertEqual("system", user.username)
                self.service.registerPatientUsingPseudonymToProject(project, nhsNumber:nhsNumber, dateBirth: self.formatter.date(from:dateBirth)!, sex:sex, registerIfNotFound: true) {
                    result in
                    switch result {
                    case .error(let error):
                        print(error.localizedDescription)
                        XCTAssertFalse(success)
                    case .success(let patient):
                        XCTAssertEqual(patient.nhsNumber, nhsNumber)
                        XCTAssertTrue(success)
                        if success {
                            if let episode = patient.episodes.filter( { (episode) in episode.project.id == project.id } ).first {
                                XCTAssertEqual(episode.project.id, project.id)
                                self.service.fetchPatientWithPseudonym(episode.pseudonym, forProject: project) {
                                    result in
                                    switch result {
                                    case .error:
                                        XCTFail()
                                    case .success(let patient2):
                                        XCTAssertEqual(patient.patientIdentifier, patient2.patientIdentifier)
                                    }
                                }
                            }
                        }
                    }
                    
                    expect.fulfill()
                }
            }
        }
        waitForExpectations(timeout: 5) { error in
            if let error = error {
                XCTFail("waitForExpectations errored: \(error)")
            }
        }
    }
    
    func testFetchPatient() {
        let project1 = Project(identifier: 69, title: "Cardiff botulinum toxin service")
        let project2 = Project(identifier: 4, title: "Cardiff botulinum toxin service")
        _fetchPatientViaPseudonym(project: project1, nhsNumber: "1111111111", dateBirth: "1965-02-24", sex: Sex.male, expectSuccess: true)
        _fetchPatientViaPseudonym(project: project2, nhsNumber: "1111111111", dateBirth: "1965-02-24", sex: Sex.male, expectSuccess: true)
        _fetchPatientViaPseudonym(project: project1, nhsNumber: "1111111111", dateBirth: "1966-02-24", sex: Sex.male, expectSuccess: false)
        _fetchPatientViaPseudonym(project: project1, nhsNumber: "1111111111", dateBirth: "1965-02-24", sex: Sex.female, expectSuccess: false)

        _fetchPatientUsingPseudonym("17b", forProject: project1, expectSuccess:true)
        _fetchPatientUsingPseudonym("17", forProject: project1, expectSuccess:false)
    }
    
    func _fetchPatientUsingPseudonym(_ pseudonym:String, forProject project:Project, expectSuccess success:Bool) {
        let expect = expectation(description: "Fetch patient using pseudonymous identifier \(pseudonym)")
        service.fetchPatientWithPseudonym(pseudonym, forProject: project) {
            result in
            switch result {
            case .error(let error):
                XCTAssertFalse(success)
                print(error.localizedDescription)
            case .success(let pt):
                XCTAssertTrue(success)
                XCTAssertEqual(pt.nhsNumber, "2222222222")
            }
            expect.fulfill()
        }
        waitForExpectations(timeout: 5) { error in
            if let error = error {
                XCTFail("waitForExpectations errored: \(error)")
            }
        }
    }
    
    
    func testNhsNumberValidation() {
        XCTAssertTrue(NhsNumber.validateNhsNumber("6328797966"))
        XCTAssertTrue(NhsNumber.validateNhsNumber("6148595893"))
        XCTAssertTrue(NhsNumber.validateNhsNumber("4823917286"))
        XCTAssertTrue(NhsNumber.validateNhsNumber("4865447040"))
        XCTAssertFalse(NhsNumber.validateNhsNumber(""))
        XCTAssertFalse(NhsNumber.validateNhsNumber("4865447041"))
        XCTAssertFalse(NhsNumber.validateNhsNumber("a4785"))
        XCTAssertFalse(NhsNumber.validateNhsNumber("1234567890"))
        XCTAssertFalse(NhsNumber.validateNhsNumber("          "))
    }
    
    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }
    
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }
    
}
